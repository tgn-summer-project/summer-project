﻿using Gamekit3D;
using TMPro;
using UnityEngine;

public class EnemyStatController : MonoBehaviour
{
    public SimpleHealthBar HealthBar;
    public GameObject DamageNumbers;

    public CharacterStat Health;
    public CharacterStat Damage;
    public CharacterStat ExpWorth;

    private Damageable damageableComponent;
    private int startHp;
    //private int level;


    void Awake()
    {
        //level = GameObject.Find("Fox").GetComponent<PlayerStats>().Level;
        damageableComponent = GetComponent<Damageable>();
    }

    void Start()
    {
        //var dmgBaseStat = new StatModifier((0.100f * level) * 1.1f, ModType.PercentAdd, this);
        //var expBaseStat = new StatModifier((0.14f * level) * 1.1f, ModType.PercentAdd, this);
        //var hpBaseStat  = new StatModifier((0.185f * level) * 1.8f, ModType.PercentAdd, this);

        //Damage.AddMod(dmgBaseStat);
        //ExpWorth.AddMod(expBaseStat);
        //Health.AddMod(hpBaseStat);
        startHp = (int)Health.Value;

        damageableComponent.maxHitPoints = startHp;
        damageableComponent.currentHitPoints = startHp;

        HealthBar.UpdateBar(startHp, startHp);

        Health.OnAddedMod.AddListener(delegate
        {
            HealthBar.UpdateBar(damageableComponent.currentHitPoints, startHp);
        });
    }

    public void BuffOnLevelUp()
    {
        //if (Health.Value < damageableComponent.maxHitPoints)
        //{
        //    Debug.Log(gameObject.name+ ": AAAAAAAA! I'm hurt and therefor can't be buffed!");
        //}

        //else
        //{
        //    var dmgBaseStat = new StatModifier((0.100f * level) * 1.1f, ModType.PercentAdd, this);
        //    var expBaseStat = new StatModifier((0.14f * level) * 1.1f, ModType.PercentAdd, this);
        //    var hpBaseStat = new StatModifier((0.185f * level) * 1.8f, ModType.PercentAdd, this);

        //    Damage.AddMod(dmgBaseStat);
        //    ExpWorth.AddMod(expBaseStat);
        //    Health.AddMod(hpBaseStat);
        //    startHp = Health.Value;

        //    damageableComponent.maxHitPoints = (int)Health.Value;
        //    damageableComponent.currentHitPoints = (int)Health.Value;

        //    HealthBar.UpdateBar(Health.Value, startHp);

        //    Health.OnAddedMod.AddListener(delegate
        //    {
        //        HealthBar.UpdateBar(damageableComponent.currentHitPoints, startHp);
        //    });
        //}
    }

    public void DecreaseHealth(float amount)
    {
        ShowNumbers(amount);
        Health.AddMod(new StatModifier(-amount, ModType.Flat, this));
    }

    public void ShowNumbers(float amount, bool criticalHit = false)
    {
        var clone = Instantiate(DamageNumbers, transform);
        clone.SetActive(true);
        var textClone = clone.GetComponentInChildren<TextMeshProUGUI>();
        textClone.text = $"{(int)amount}";
        Destroy(clone, 1);
    }
}
