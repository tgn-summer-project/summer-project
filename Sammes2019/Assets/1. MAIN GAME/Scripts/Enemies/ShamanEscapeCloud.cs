﻿using Gamekit3D;
using UnityEngine;
using UnityEngine.AI;

public class ShamanEscapeCloud : MonoBehaviour
{
    [Header("Shamans Escape Cloud")]
    public GameObject shamanMaster;
    public GameObject player;
    public GameObject[] shamanZones;
    public bool escaping;
    public float escapeSpeed;
    public float securityCheck;
    public GameObject rndZone;
    public GameObject lastZone;
    public Vector3 currentZone;

    // Start is called before the first frame update
    void Awake()
    {
        securityCheck = shamanMaster.GetComponent<ShamanBehaviour>().fleeingDistance;
        player = GameObject.Find("Fox");
        escaping = false;
        UpdateEscapeRoute();
    }

    void UpdateEscapeRoute()
    {
        Vector3 rndPosWithin;
        bool allowEscapeRoute = false;

        while (!allowEscapeRoute)
        {
            rndZone = shamanZones[UnityEngine.Random.Range(0, shamanZones.Length)];

            if (lastZone == rndZone)
            {
                rndZone = shamanZones[UnityEngine.Random.Range(0, shamanZones.Length)];
            }

            else
            {
                allowEscapeRoute = true;
            }
        }

        rndPosWithin = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
        rndPosWithin = rndZone.transform.TransformPoint(rndPosWithin * .5f);

        lastZone = rndZone;
        currentZone = rndZone.transform.position + new Vector3(0, 0.5f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(!escaping)
        {
            UpdateEscapeRoute();
            escaping = true;
        }

        float step = escapeSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, currentZone, step);

        // Check if the position of the cube and sphere are approximately equal.
        if (Vector3.Distance(transform.position, currentZone) < 0.03f)
        {
            securityCheck = shamanMaster.GetComponent<ShamanBehaviour>().fleeingDistanceStart;

            //If the player is too close, keep escaping to another point
            if (Vector3.Distance(transform.position, player.transform.position) < securityCheck)
            {
                UpdateEscapeRoute();
            }

            else
            {
                shamanMaster.transform.position = gameObject.transform.position - new Vector3(0, 0.5f, 0);
                shamanMaster.SetActive(true);
                shamanMaster.GetComponent<ShamanBehaviour>().timeToRun = shamanMaster.GetComponent<ShamanBehaviour>().fleeingTimer;
                escaping = false;
                gameObject.SetActive(false);
            }
        }
    }
}
