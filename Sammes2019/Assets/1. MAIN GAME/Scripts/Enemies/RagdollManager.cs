﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    public float timeToDespawn;
    private float timer;

    private void Awake()
    {
        timer = timeToDespawn;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0)
            Destroy(gameObject);
    }
}