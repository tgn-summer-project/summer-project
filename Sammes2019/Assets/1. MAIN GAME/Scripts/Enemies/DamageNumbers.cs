﻿using UnityEngine;

public class DamageNumbers : MonoBehaviour
{
    public float Speed = 0.5f;
    void Update()
    {
        transform.position += Vector3.up * Time.deltaTime * Speed;
    }
}
