﻿using Gamekit3D.Message;
using Gamekit3D.WorldBuilding;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace Gamekit3D
{
    [DefaultExecutionOrder(100)]
    public class ShamanBehaviour : MonoBehaviour, IMessageReceiver
    {
        public static readonly int hashVerticalDot = Animator.StringToHash("VerticalHitDot");
        public static readonly int hashHorizontalDot = Animator.StringToHash("HorizontalHitDot");
        public static readonly int hashThrown = Animator.StringToHash("Thrown");
        public static readonly int hashHit = Animator.StringToHash("Hit");
        public static readonly int hashAttack = Animator.StringToHash("Attack");
        public static readonly int hashHaveEnemy = Animator.StringToHash("HaveTarget");
        public static readonly int hashFleeing = Animator.StringToHash("Fleeing");

        public static readonly int hashIdleState = Animator.StringToHash("Idle");

        public TargetScanner playerScanner;
        public float fleeingDistanceStart;
        public float fleeingDistance;
        public float fleeingTimer;
        public float timeToRun;
        public RangeWeapon rangeWeapon;

        [Header("Nackens AI Settings")]
        public GameObject escapeCloud;
        public GameObject minionToSpawn;

        [Header("Audio")]
        public RandomAudioPlayer attackAudio;
        public RandomAudioPlayer frontStepAudio;
        public RandomAudioPlayer backStepAudio;
        public RandomAudioPlayer hitAudio;
        public RandomAudioPlayer gruntAudio;
        public RandomAudioPlayer deathAudio;
        public RandomAudioPlayer spottedAudio;

        public EnemyController controller => m_Controller;
        public PlayerController target => m_Target;

        protected PlayerController m_Target;
        protected EnemyController m_Controller;
        protected GameMaster gameMaster;
        protected EnemyStatController enemyStatController;
        protected bool m_Fleeing;

        protected Vector3 m_RememberedTargetPosition;

        protected void OnEnable()
        {
            m_Controller = GetComponentInChildren<EnemyController>();

            m_Controller.animator.Play(hashIdleState, 0, Random.value);

            fleeingDistance = fleeingDistanceStart;
            timeToRun = fleeingTimer;

            gameMaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
            enemyStatController = GetComponent<EnemyStatController>();

            SceneLinkedSMB<ShamanBehaviour>.Initialise(m_Controller.animator, this);

            escapeCloud.SetActive(false);
        }

        public void OnReceiveMessage(Message.MessageType type, object sender, object msg)
        {
            switch (type)
            {
                case Message.MessageType.DEAD:
                    Death((Damageable.DamageMessage)msg);
                    break;
                case Message.MessageType.DAMAGED:
                    ApplyDamage((Damageable.DamageMessage)msg);
                    break;
                default:
                    break;
            }
        }

        public void Death(Damageable.DamageMessage msg)
        {
            var exp = enemyStatController.ExpWorth.Value;
            gameMaster.EnemyKilled(exp);
            Vector3 pushForce = transform.position - msg.damageSource;

            pushForce.y = 0;

            transform.forward = -pushForce.normalized;
            controller.AddForce(pushForce.normalized * 7.0f - Physics.gravity * 0.6f);

            controller.animator.SetTrigger(hashHit);
            controller.animator.SetTrigger(hashThrown);

            //We unparent the deathAudio source, as it would destroy it with the gameobject when it get replaced by the ragdol otherwise
            deathAudio.transform.SetParent(null, true);
            deathAudio.PlayRandomClip();

            Destroy(deathAudio, deathAudio.clip == null ? 0.0f : deathAudio.clip.length + 0.5f);
        }

        public void ApplyDamage(Damageable.DamageMessage msg)
        {
            //TODO: Change to ranged weapon, can't be hurt by normal as it will escape
            if (msg.damager.name == "Staff")
                CameraShake.Shake(0.06f, 0.1f);

            float verticalDot = Vector3.Dot(Vector3.up, msg.direction);
            float horizontalDot = Vector3.Dot(transform.right, msg.direction);

            controller.animator.SetFloat(hashVerticalDot, verticalDot);
            controller.animator.SetFloat(hashHorizontalDot, horizontalDot);

            controller.animator.SetTrigger(hashHit);

            hitAudio.PlayRandomClip();
        }

        public void Shoot()
        {
            rangeWeapon.Attack(m_RememberedTargetPosition);
        }

        public void TriggerAttack()
        {
            m_Controller.animator.SetTrigger(hashAttack);
        }

        public void RememberTargetPosition()
        {
            if (m_Target == null)
                return;

            m_RememberedTargetPosition = m_Target.transform.position;
        }

        void PlayStep(int frontFoot)
        {
            if (frontStepAudio != null && frontFoot == 1)
                frontStepAudio.PlayRandomClip();
            else if (backStepAudio != null && frontFoot == 0)
                backStepAudio.PlayRandomClip();
        }

        public void Grunt()
        {
            if (gruntAudio != null)
                gruntAudio.PlayRandomClip();
        }

        public void Spotted()
        {
            if (spottedAudio != null)
                spottedAudio.PlayRandomClip();
        }

        public void CheckNeedFleeing()
        {
            if (m_Target == null)
            {
                m_Fleeing = false;
                return;
            }

            //Distance from player
            float fromTarget = Vector3.Distance(transform.position, m_Target.transform.position);

            //The player is too close, escape to another place
            if (fromTarget <= fleeingDistance || timeToRun <= 0)
            {
                escapeCloud.SetActive(true);
                escapeCloud.transform.position = gameObject.transform.position + new Vector3(0, 0.5f, 0);
                fleeingDistance = 0.2f;
                gameObject.SetActive(false);
            }
        }

        public void FindTarget()
        {
            //we ignore height difference if the target was already seen
            m_Target = playerScanner.Detect(transform, m_Target == null);
            m_Controller.animator.SetBool(hashHaveEnemy, m_Target != null);
        }

        public void Update()
        {
            if (timeToRun >= 0)
            {
                timeToRun -= Time.deltaTime;
            }

            CheckNeedFleeing();
        }
    }
    /*
#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        playerScanner.EditorGizmo(transform);
    }
#endif*/
}

/*
#if UNITY_EDITOR
    [CustomEditor(typeof(ShamanBehaviour))]
    public class ShamanBehaviourEditor : Editor
    {
        ShamanBehaviour m_Target;

        void OnEnable()
        {
            m_Target = target as ShamanBehaviour;
        }

        public override void OnInspectorGUI()
        {
            if (m_Target.playerScanner.detectionRadius < m_Target.fleeingDistance)
            {
                EditorGUILayout.HelpBox("The scanner detection radius is smaller than the fleeing range.\n" +
                    "The Shaman will never shoot at the player as it will flee past the range at which it can see the player",
                    MessageType.Warning, true);
            }

            base.OnInspectorGUI();
        }
    }

#endif
}*/