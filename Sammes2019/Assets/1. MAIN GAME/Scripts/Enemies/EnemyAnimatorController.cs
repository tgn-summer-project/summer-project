﻿using UnityEngine;

public class EnemyAnimatorController : MonoBehaviour
{
    private Animator animatorComp;
    private SkinnedMeshRenderer mesh;
    private Transform playerTransform;

    void Awake()
    {
        playerTransform = GameObject.Find("Fox").GetComponent<Transform>();
        animatorComp = GetComponent<Animator>();
        mesh = GetComponentInChildren<SkinnedMeshRenderer>();
    }
    private static bool IsObjectVisible(Camera @this, Renderer renderer)
    {
        return GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(@this), renderer.bounds);
    }

    void FixedUpdate()
    {
        var isVisibleForCamera = IsObjectVisible(Camera.main, mesh);
        var isNearPlayer = Vector3.Distance(playerTransform.position, transform.position) <= 40;

        animatorComp.enabled = isVisibleForCamera || isNearPlayer;
    }
}
