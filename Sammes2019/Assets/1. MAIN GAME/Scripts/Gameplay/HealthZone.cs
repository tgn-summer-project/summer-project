﻿using Gamekit3D;
using UnityEngine;

public class HealthZone : MonoBehaviour
{
    public SimpleHealthBar Healthbar;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name != "Fox")
            return;

        var playerStats = collider.gameObject.GetComponent<PlayerStats>();
        var damageable = collider.gameObject.GetComponent<Damageable>();

        var maxHealth = playerStats.MaxHealth.Value;
        var currentHealth = damageable.currentHitPoints;

        var hpRecovery = maxHealth - currentHealth;

        playerStats.Health.AddMod(new StatModifier(hpRecovery, ModType.Flat, this));

        damageable.currentHitPoints = playerStats.Health.Value;

        Healthbar.UpdateBar(maxHealth, maxHealth);

        Destroy(gameObject);
    }
}