﻿using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public static class DataManager
{
    [Header("File Data")]
    public static string gameFileName;
    public static PlayerData gameData;
    public static SettingsData settingsData;

    [Header("Player Stats")]
    public static GameObject player;
    public static PlayerStats ps;

    [Header("Save Files Button")]
    //public static TMPro.TextMeshProUGUI lvl;
    public static TMPro.TextMeshProUGUI round;
    public static TMPro.TextMeshProUGUI gameStats;

    //CALLED UPON GAME START, SET THE REF SO THE DATA MANAGER KNOWS WHAT TO AFFECT
    public static void AssignFileRef(string fileRef)
    {
        gameFileName = fileRef;
    }

    //SAVE SETTINGS FOR CONFIG ON LAUNCH
    public static void SaveSettingsData()
    {
        if (SceneManager.GetActiveScene().name == "MainMenuFox")
        {
            MainMenuLogic menuSettings = GameObject.Find("MainMenu").GetComponent<MainMenuLogic>();

            BinaryFormatter dataHandler = new BinaryFormatter();
            string path = Application.persistentDataPath + "/Config.gsc";
            FileStream stream = new FileStream(path, FileMode.Create);

            Debug.Log("FILE WAS CREATED / MODIFIED!");

            settingsData = new SettingsData(menuSettings);

            Debug.LogWarning("MASTER: " + settingsData.masterSliderLevel);
            Debug.LogWarning("MUSIC: " + settingsData.musicSliderLevel);
            Debug.LogWarning("SFX: " + settingsData.sfxSliderLevel);
            Debug.LogWarning("AMB: " + settingsData.ambientSliderLevel);

            Debug.Log("SETTINGS HAVE BEEN CONFIRMED");

            dataHandler.Serialize(stream, settingsData);
            stream.Close();

            Debug.Log("SETTINGS DATA HAS BEEN SAVED!");
        }

        else if (SceneManager.GetActiveScene().name == "Level 2")
        {
            PauseMenuLogic pauseSettings = GameObject.Find("GameMaster").GetComponent<PauseMenuLogic>();

            BinaryFormatter dataHandler = new BinaryFormatter();
            string path = Application.persistentDataPath + "/Config.gsc";
            FileStream stream = new FileStream(path, FileMode.Create);

            Debug.Log("FILE WAS CREATED / MODIFIED!");

            settingsData = new SettingsData(pauseSettings);

            Debug.Log("LOADED VALUE MUSIC: " + settingsData.musicSliderLevel);

            Debug.Log("SETTINGS HAVE BEEN CONFIRMED");

            dataHandler.Serialize(stream, settingsData);
            stream.Close();

            Debug.Log("SETTINGS DATA HAS BEEN SAVED!");
        }
    }

    //GET DATA FROM PLAYER, CONVERT IF NEEDED
    public static void SaveGameData()
    {
        player = GameObject.Find("Fox");
        ps = player.GetComponent<PlayerStats>();

        BinaryFormatter dataHandler = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + gameFileName + ".fox";
        FileStream stream = new FileStream(path, FileMode.Create);

        Debug.Log("FILE WAS CREATED / MODIFIED!");

        gameData = new PlayerData(ps);

        Debug.Log("DATA HAS BEEN SET FROM PS TO DATA");

        dataHandler.Serialize(stream, gameData);
        stream.Close();

        Debug.Log("DATA HAS BEEN SAVED!");
    }

    //LOAD SETTINGS DATA
    public static void LoadSettingsData()
    {
        if (SceneManager.GetActiveScene().name == "MainMenuFox")
        {
            MainMenuLogic menuSettings = GameObject.Find("MainMenu").GetComponent<MainMenuLogic>();

            string path = Application.persistentDataPath + "/Config.gsc";
            if (File.Exists(path))
            {
                BinaryFormatter dataHandler = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                settingsData = dataHandler.Deserialize(stream) as SettingsData;

                //Load sound settings
                menuSettings.masterLevel = settingsData.masterSliderLevel;
                menuSettings.musicLevel = settingsData.musicSliderLevel;
                menuSettings.soundFXLevel = settingsData.sfxSliderLevel;
                menuSettings.ambientLevel = settingsData.ambientSliderLevel;

                //Load graphics settings
                menuSettings.resolutionCurrent.width = settingsData.resWidth;
                menuSettings.resolutionCurrent.height = settingsData.resHeight;
                menuSettings.hertz = settingsData.hertzSettings;
                menuSettings.qualityGrade = settingsData.qualitySettings;
                menuSettings.screenModeChoice = settingsData.fullscreenSettings;

                stream.Close();

                Debug.Log("SETTINGS DATA HAS BEEN LOADED!");
            }
        }

        else if(SceneManager.GetActiveScene().name == "Level 2")
        {
            PauseMenuLogic pauseSettings = GameObject.Find("GameMaster").GetComponent<PauseMenuLogic>();

            string path = Application.persistentDataPath + "/Config.gsc";
            if (File.Exists(path))
            {
                BinaryFormatter dataHandler = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                settingsData = dataHandler.Deserialize(stream) as SettingsData;

                //Save sound settings
                pauseSettings.masterLevel = settingsData.masterSliderLevel;
                pauseSettings.musicLevel = settingsData.musicSliderLevel;
                pauseSettings.soundFXLevel = settingsData.sfxSliderLevel;
                pauseSettings.ambientLevel = settingsData.ambientSliderLevel;

                //Save graphics settings
                pauseSettings.resolutionCurrent.width = settingsData.resWidth;
                pauseSettings.resolutionCurrent.height = settingsData.resHeight;
                pauseSettings.hertz = settingsData.hertzSettings;
                pauseSettings.qualityGrade = settingsData.qualitySettings;
                pauseSettings.screenModeChoice = settingsData.fullscreenSettings;

                stream.Close();

                Debug.Log("SETTINGS DATA HAS BEEN LOADED!");
            }
        }

        else
        {
            Debug.LogError("Config.gsc was not found! </3 ");
        }
    }

    //LOAD DATA TO THE PLAYER
    public static void LoadGameData()
    {
        player = GameObject.Find("Fox");
        ps = player.GetComponent<PlayerStats>();

        //Search for corrupt file, delete it if found
        string path = Application.persistentDataPath + "/.fox";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        //Search for the game file
        path = Application.persistentDataPath + "/" + gameFileName + ".fox";
        if (File.Exists(path))
        {
            BinaryFormatter dataHandler = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            gameData = dataHandler.Deserialize(stream) as PlayerData;

            ps.timerH = gameData.timerH;
            ps.timerM = gameData.timerM;
            ps.timerS = gameData.timerS;
            ps.roundCompleted = gameData.round;
            ps.time = gameData.time;
            ps.killCount = gameData.killCount;
            ps.AvailableSkillPoints = gameData.skillPointsToSpend;
            ps.Experience = gameData.xp;
            ps.ExpRequired = gameData.xpReq;
            ps.Level = gameData.lvl;
            ps.Health.BaseValue = gameData.hp;
            ps.MaxHealth.BaseValue = gameData.hpMax;
            ps.RunningSpeed.BaseValue = gameData.runSpeed;
            ps.Critchance.BaseValue = gameData.critChance;
            ps.AttackSpeed.BaseValue = gameData.attackSpeed;
            ps.RangedDamage.BaseValue = gameData.rangedDMG;
            ps.MeleeDamage.BaseValue = gameData.meleeDMG;
            ps.Stamina.BaseValue = gameData.dexterity;
            ps.Intelligence.BaseValue = gameData.intelligence;
            ps.Strength.BaseValue = gameData.strength;

            stream.Close();

            Debug.Log("DATA HAS BEEN LOADED!");
        }

        else
        {
            Debug.LogError(gameFileName + ".fox was not found! </3 ");
        }
    }

    //LOAD DATA ON APPLICATION START AND SET TO GAME BUTTONS
    public static void LoadGameOnStart()
    {
        for (int i = 1; i < 4; i++)
        {
            gameFileName = "File " +i.ToString();

            string path = Application.persistentDataPath + "/" + gameFileName + ".fox";

            if (File.Exists(path))
            {
                Debug.Log("FILE WAS FOUND!");

                BinaryFormatter dataHandler = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                Debug.Log("STREAM IS OPEN");

                gameData = dataHandler.Deserialize(stream) as PlayerData;

                Debug.Log("DATA LINKED");

                //lvl = GameObject.Find("PLAY_F" +i.ToString()+ "_DataLevel").GetComponent<TMPro.TextMeshProUGUI>();
                round = GameObject.Find("PLAY_F" + i.ToString() + "_DataRound").GetComponent<TMPro.TextMeshProUGUI>();
                gameStats = GameObject.Find("PLAY_F" + i.ToString() + "_DataStats").GetComponent<TMPro.TextMeshProUGUI>();

                Debug.Log("DATA SET TO UI");

                //lvl.text = "Level " + gameData.lvl.ToString();
                round.text = "Round " + gameData.round.ToString();
                gameStats.text = "Time: " + gameData.time+ "\n Kill Count: " +gameData.killCount;

                stream.Close();

                Debug.Log("DONE!!!");
            }

            else
            {
                Debug.LogError(gameFileName + ".fox was not found! </3 ");

                //lvl = GameObject.Find("PLAY_F" + i.ToString() + "_DataLevel").GetComponent<TMPro.TextMeshProUGUI>();
                round = GameObject.Find("PLAY_F" + i.ToString() + "_DataRound").GetComponent<TMPro.TextMeshProUGUI>();
                gameStats = GameObject.Find("PLAY_F" + i.ToString() + "_DataStats").GetComponent<TMPro.TextMeshProUGUI>();

                //lvl.text = "Level 0";
                round.text = "Round 0";
                gameStats.text = "EMPTY GAME FILE";
            }
        }
    }

    //DELETE DATA FILE
    public static void DeleteFile(string fileRef)
    {
        string fileLocation = Application.persistentDataPath + "/" + fileRef + ".fox";
        if (File.Exists(fileLocation))
        {
            File.Delete(fileLocation);
            Debug.Log(fileRef+ " has been deleted");
        }

        else
        {
            Debug.LogError(fileRef + " was not found! </3 ");
        }
    }
}