using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManualSpawnActivate : MonoBehaviour{
	public GameObject spawnArea1;
	public GameObject spawnArea2;
	public GameObject spawnArea3;
	public GameObject spawnArea4;
	public GameObject spawnArea5;
	public GameObject spawnArea6;
	void Update(){
		if(Input.GetButtonDown("Spawn1"))
		{
			spawnArea1.SetActive(true);
		}
		if(Input.GetButtonDown("Spawn2"))
		{
			spawnArea2.SetActive(true);
		}
		if(Input.GetButtonDown("Spawn3"))
		{
			spawnArea3.SetActive(true);
		}
		if(Input.GetButtonDown("Spawn4"))
		{
			spawnArea4.SetActive(true);
		}
		if(Input.GetButtonDown("Spawn5"))
		{
			spawnArea5.SetActive(true);
		}
		if(Input.GetButtonDown("Spawn6"))
		{
			spawnArea6.SetActive(true);
		}
	}
}