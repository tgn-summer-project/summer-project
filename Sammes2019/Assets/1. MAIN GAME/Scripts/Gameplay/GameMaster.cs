﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour
{
    public UnityEvent OnEnemyKill;
    public GameOverLogic goRef;
    public WinMenuLogic winRef;
    public PauseMenuLogic pauseRef;
    public bool gameOver;
    public bool win;
    public AudioClip endSong;

    public float transSpeed;
    public AudioSource peaceSource;
    public AudioSource warSource;
    public bool peaceTimes;
    public bool warTimes;
    public bool musicTrans;
    private List<GameObject> enemies = new List<GameObject>();

    [HideInInspector]
    public int TotalKillCount;

    public int h;
    public int m;
    public float sec;

    [HideInInspector]
    public PlayerStats playerStats;
    private EnemyStatController currentModified;

    [HideInInspector]
    public string CurrentLevel => SceneManager.GetActiveScene().name;

    [HideInInspector]
    public int LevelCount = 1;

    public void EnemyKilled(float expGained)
    {
        TotalKillCount++;
        OnEnemyKill.Invoke();
        playerStats.killCount++;
        //playerStats.AddExperience(expGained);
    }

    void Awake()
    {
        win = false;
        gameOver = false;
        peaceTimes = true;
        warTimes = false;
        musicTrans = false;

        if (gameObject.transform.Find("MusicLibrary/Peace"))
        {
            peaceSource = gameObject.transform.Find("MusicLibrary/Peace").GetComponent<AudioSource>();
        }

        if (gameObject.transform.Find("MusicLibrary/War"))
        {
            warSource = gameObject.transform.Find("MusicLibrary/War").GetComponent<AudioSource>();
        }

        if (GameObject.Find("Fox"))
        {
            var player = GameObject.Find("Fox");
            playerStats = player.GetComponent<PlayerStats>();
            DataManager.LoadGameData();

            if(playerStats.roundCompleted >= 1)
            {
                Debug.Log("This is not the first time you play! KEEP GOING!");

                h = playerStats.timerH;
                m = playerStats.timerM;
                sec = playerStats.timerS;
            }

            else
            {
                Debug.Log("This is the first time you play!");
            }
        }

        /*if(gameObject.Find("PlayerUI/Healhbar/Bar"))
        {
            hpBar = gameObject.Find("PlayerUI/Healhbar/Bar")
        }*/
    }

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        peaceSource.volume = 1.0f;
        warSource.volume = 0.0f;
    }

    public void SwitchMusicMode()
    {
        //Fade from peace to war
        if (peaceTimes)
        {
            peaceTimes = false;
            warTimes = true;
            musicTrans = true;

            Debug.Log("SWITCHING MODE TO WAR");
        }

        //Fade from war to peace
        else if (warTimes)
        {
            warTimes = false;
            peaceTimes = true;
            musicTrans = true;

            Debug.Log("SWITCHING MODE TO PEACE");
        }
    }

    public void LevelComplete()
    {
        win = true;
        playerStats.roundCompleted++;
        goRef.enabled = false;
        pauseRef.enabled = false;
        Destroy(GameObject.Find("GameOver"));

        if (GameObject.Find("BossHealthbar"))
        {
            Destroy(GameObject.Find("BossHealthbar"));
        }

        peaceSource.Stop();
        peaceSource.clip = endSong;
        peaceSource.Play();
        winRef.WinnerWinnerChickenDinner(h, m, sec);
    }

    public void GameOver()
    {
        if (!win)
        {
            gameOver = true;
            pauseRef.enabled = false;
            Destroy(GameObject.Find("PauseMenu"));
            Destroy(GameObject.Find("BossHealthbar"));
            goRef.GameOver(h, m, sec);
        }
    }

    //Keeps track of game time
    private void Update()
    {
        sec += Time.deltaTime;

        if (sec >= 60)
        {
            m++;
            sec = 0;
            Destroy(GameObject.Find("DeathSource"));
        }

        else if (m >= 60)
        {
            h++;
            m = 0;
        }

        if (gameOver == false)
        {
            playerStats.time = h + "h " + m + "min " + Mathf.RoundToInt(sec) + "sec";
        }

        if (musicTrans == true)
        {
            //Switch to war
            if (warTimes)
            {
                peaceSource.volume -= Time.deltaTime * transSpeed;
                warSource.volume += Time.deltaTime * transSpeed;

                if (peaceSource.volume <= 0 && warSource.volume >= 1)
                {
                    Debug.Log("TRANSITION COMPLETE. WAR MODE 100%");
                    musicTrans = false;
                }
            }

            //Switch to peace
            else if (peaceTimes)
            {
                peaceSource.volume += Time.deltaTime * transSpeed;
                warSource.volume -= Time.deltaTime * transSpeed;

                if (peaceSource.volume >= 1 && warSource.volume <= 0)
                {
                    Debug.Log("TRANSITION COMPLETE. PEACE MODE 100%");
                    musicTrans = false;
                }
            }
        }

        //Buff enemies upon level up
        /*if (playerStats.hasLeveledUp)
        {
            enemies.Clear();
            enemies.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));

            Debug.Log(enemies.Count+ " enemies are currently alive as of level up!");

            for(int i = 0; i < enemies.Count; i++)
            {
                currentModified = enemies[i].GetComponent<EnemyStatController>();
                currentModified.BuffOnLevelUp();
            }

            Debug.Log("Buffing enemies!");

            playerStats.hasLeveledUp = false;
        }*/
    }
}
