﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WaveSpawner : MonoBehaviour
{
    [HideInInspector]
    public int TotalEnemyCount;

    private List<GameObject> allEnemies;

    [System.Serializable]
    public class Wave
    {
        public string Name;
        public int EnemyCount = 5;
        public int DelayUntilNextWave = 15;
        public float SpawnIntervalMin = 1;
        public float SpawnIntervalMax = 3;
        public bool RandomizePosition = true;
        public GameObject Boss;
        public GameObject[] EnemyType;
    }

    public Wave[] Waves;
    public UnityEvent OnAllEnemiesKilled;

    private Transform[] initialSpawnPoints;

    private int spawnedEnemies;
    private int currentWave;
    private float nextWaveSpawn;
    private bool isSpawning;

    void Start()
    {
        allEnemies = new List<GameObject>();
        initialSpawnPoints = gameObject.GetComponentsInChildren<Transform>().Where(x => x.transform.position != Vector3.zero).ToArray();

        foreach (var wave in Waves)
        {
            TotalEnemyCount += wave.EnemyCount;
        }
    }

    void Update()
    {
        for (int i = allEnemies.Count - 1; i >= 0; --i)
        {
            if (allEnemies[i] == null)
            {
                allEnemies.RemoveAt(i);
            }
        }

        if (allEnemies.Count <= 0 && spawnedEnemies >= TotalEnemyCount)
        {
            this.gameObject.SetActive(false);
            OnAllEnemiesKilled.Invoke();
            return;
        }

        if (currentWave >= Waves.Length )
        {
           return;
        }

        if (nextWaveSpawn <= 0 && !isSpawning)
        {
            nextWaveSpawn = Time.deltaTime + Waves[currentWave].DelayUntilNextWave;
            StartCoroutine(Spawn());
        }
        else
        {
            nextWaveSpawn -= Time.deltaTime;
        }
    }

    private IEnumerator Spawn()
    {
        isSpawning = true;
        var wave = Waves[currentWave];
        var randomizePos = wave.RandomizePosition;

        for (int i = 0; i < wave.EnemyCount; i++)
        {
            var randomEnemy = Random.Range(0, wave.EnemyType.Length);
            var enemyObj = wave.EnemyType[randomEnemy];

            var boss = wave.Boss;
            if (boss != null && Random.Range(0, 100) >= 85)
            {
                enemyObj = boss;
            }

            var randomSpawn = Random.Range(0, initialSpawnPoints.Length);
            var spawn = initialSpawnPoints[randomSpawn].position;
            if (randomizePos)
            {
                spawn = ShakePosition(spawn);
            }

            var clone = Instantiate(enemyObj, spawn, Quaternion.identity);
            allEnemies.Add(clone);

            spawnedEnemies++;

            var wait = new WaitForSeconds(Random.Range(wave.SpawnIntervalMin, wave.SpawnIntervalMax));
            yield return wait;
        }

        isSpawning = false;
        currentWave++;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Color c = new Color(0, 0, 0.7f, 0.4f);
        foreach (var spawnPoint in gameObject.GetComponentsInChildren<Transform>())
        {
            Gizmos.color = c;
            Gizmos.DrawSphere(spawnPoint.position, 1);
        }
    }

    #endif

    private static Vector3 ShakePosition(Vector3 origin)
    {
        origin.x += Random.Range(-3, 3);
        origin.z += Random.Range(-3, 3);
        return origin;
    }
}
