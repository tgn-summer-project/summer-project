﻿using UnityEngine;

public class CameraScrollZoom : MonoBehaviour
{
    public float Min = 15f;
    public float Max = 90f;
    public float Sensitivity = 10f;
    private Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        var fov = camera.fieldOfView;
        fov += -Input.GetAxis("Mouse ScrollWheel") * Sensitivity;
        fov = Mathf.Clamp(fov, Min, Max);
        Camera.main.fieldOfView = fov;
    }
}
