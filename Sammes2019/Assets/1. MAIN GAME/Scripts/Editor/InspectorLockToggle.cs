﻿using UnityEditor;

static class InspectorLockToggle
{
    [MenuItem("Custom/Toggle Inspector Lock %l")] 
    static void ToggleInspectorLock()
    {
        ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }
}