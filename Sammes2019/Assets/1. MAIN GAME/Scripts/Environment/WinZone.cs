﻿using UnityEngine;

public class WinZone : MonoBehaviour
{
    public GameMaster gmRef;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            gmRef.LevelComplete();
        }
    }
}
