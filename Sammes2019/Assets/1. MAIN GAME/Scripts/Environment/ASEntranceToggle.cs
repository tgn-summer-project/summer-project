﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASEntranceToggle : MonoBehaviour
{
    public Transform northern;
    public Transform southern;

    public GameObject AmbientSwitch;

    void Awake()
    {
        gameObject.transform.position = northern.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Fox")
        {
            if (gameObject.transform.position == southern.position && AmbientSwitch.GetComponent<AmbientSwitchLogic>().northern)
            {
                gameObject.transform.position = northern.position;
                AmbientSwitch.GetComponent<AmbientSwitchLogic>().SwitchEntrance();
            }

            else if (gameObject.transform.position == northern.position && AmbientSwitch.GetComponent<AmbientSwitchLogic>().southern)
            {
                gameObject.transform.position = southern.position;
                AmbientSwitch.GetComponent<AmbientSwitchLogic>().SwitchEntrance();
            }
        }
    }
}