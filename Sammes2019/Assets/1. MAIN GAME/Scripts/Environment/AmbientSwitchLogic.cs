﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AmbientSwitchLogic : MonoBehaviour
{
    [Header("Ambient Sound Settings")]
    public AudioSource ambientSource;
    //public List<AudioSource> audioSources = new List<AudioSource>();

    public AudioClip forestAmb;
    public AudioClip caveAmb;

    [Header("Ambient Trigger Settings")]
    public Transform forestTriggerNorthern;
    public Transform forestTriggerSouthern;
    public Transform caveTriggerNorthern;
    public Transform caveTriggerSouthern;

    [Header("Ambient Location Settings")]
    public bool forest;
    public bool cave;

    [Header("Ambient Entrance Settings")]
    public bool northern;
    public bool southern;

    private bool fadeIn;
    private bool fadeOut;

    // Start is called before the first frame update
    void Awake()
    {
        forest = true;
        cave = false;
        northern = false;
        southern = true;

        gameObject.transform.position = caveTriggerSouthern.position;
        gameObject.transform.rotation = caveTriggerSouthern.rotation;

        /*foreach (AudioSource aSource in audioSources)
        {
            if (aSource.name == "MusicSource" || aSource.name == "AmbientSource")
            {
                Debug.Log("Skipped cause not needed with Echo");
            }

            else
            {
                aSource.gameObject.GetComponent<AudioEchoFilter>().enabled = false;
                Debug.Log(aSource.name + " got Echo component added!");
            }
        }*/
    }

    void SwitchAmb()
    {
        if (forest)
        {
            forest = false;
            cave = true;
            fadeOut = true;

            /*audioSources.Clear();
            audioSources.AddRange(FindObjectsOfType<AudioSource>());

            foreach (AudioSource aSource in audioSources)
            {
                if (aSource.name == "MusicSource" || aSource.name == "AmbientSource")
                {
                    Debug.Log("Skipped cause not needed with Echo");
                }

                else
                {
                    aSource.gameObject.GetComponent<AudioEchoFilter>().enabled = true;
                    Debug.Log(aSource.name + " got Echo component activated!");
                }
            }*/
        }

        else if (cave)
        {
            cave = false;
            forest = true;
            fadeOut = true;

            /*foreach (AudioSource aSource in audioSources)
            {
                if (aSource.name == "MusicSource" || aSource.name == "AmbientSource")
                {
                    Debug.Log("Skipped cause not needed with Echo");
                }

                else
                {
                    aSource.gameObject.GetComponent<AudioEchoFilter>().enabled = false;
                    Debug.Log(aSource.name + " got Echo component deactivated!");
                }
            }*/
        }
    }

    public void SwitchEntrance()
    {
        if (southern)
        {
            southern = false;
            northern = true;

            if (forest && northern)
            {
                gameObject.transform.position = caveTriggerNorthern.position;
                gameObject.transform.rotation = caveTriggerNorthern.rotation;
            }

            else if (cave && northern)
            {
                gameObject.transform.position = forestTriggerNorthern.position;
                gameObject.transform.rotation = forestTriggerNorthern.rotation;
            }
        }

        else if (northern)
        {
            northern = false;
            southern = true;

            if (forest && southern)
            {
                gameObject.transform.position = caveTriggerSouthern.position;
                gameObject.transform.rotation = caveTriggerSouthern.rotation;
            }

            else if (cave && southern)
            {
                gameObject.transform.position = forestTriggerSouthern.position;
                gameObject.transform.rotation = forestTriggerSouthern.rotation;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Fox")
        {
            SwitchAmb();
        }
    }

    void Update()
    {
        if (fadeOut)
        {
            //Check if we exit or enter the cave, aswell as if it's the northern or southern entrance
            if (forest && southern)
            {
                gameObject.transform.position = caveTriggerSouthern.position;
                gameObject.transform.rotation = caveTriggerSouthern.rotation;
            }

            else if(cave && southern)
            {
                gameObject.transform.position = forestTriggerSouthern.position;
                gameObject.transform.rotation = forestTriggerSouthern.rotation;
            }

            else if (forest && northern)
            {
                gameObject.transform.position = caveTriggerNorthern.position;
                gameObject.transform.rotation = caveTriggerNorthern.rotation;
            }

            else if (cave && northern)
            {
                gameObject.transform.position = forestTriggerNorthern.position;
                gameObject.transform.rotation = forestTriggerNorthern.rotation;
            }

            //Change the audio
            ambientSource.volume -= Time.deltaTime * 0.5f;

            if (ambientSource.volume <= 0 && forest)
            {
                fadeOut = false;
                ambientSource.Stop();
                ambientSource.clip = forestAmb;
                ambientSource.Play();
                fadeIn = true;
            }

            else if (ambientSource.volume <= 0 && cave)
            {
                fadeOut = false;
                ambientSource.Stop();
                ambientSource.clip = caveAmb;
                ambientSource.Play();
                fadeIn = true;
            }
        }

        else if (fadeIn)
        {
            ambientSource.volume += Time.deltaTime * 0.5f;

            if ((ambientSource.volume >= 1 && forest) || (ambientSource.volume >= 1 && cave))
            {
                fadeIn = false;
            }
        }
    }
}