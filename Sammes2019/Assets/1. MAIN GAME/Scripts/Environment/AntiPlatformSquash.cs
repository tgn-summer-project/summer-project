﻿using UnityEngine;

public class AntiPlatformSquash : MonoBehaviour
{
    public GameObject platform;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Fox")
        {
            Physics.IgnoreCollision(platform.GetComponent<BoxCollider>(), other, true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Physics.IgnoreCollision(platform.GetComponent<BoxCollider>(), other, false);
    }
}