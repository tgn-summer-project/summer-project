﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
    private CharacterController characterController;
    private Vector3 _velocity;

    public Vector3 Force = new Vector3(0, 50, 0);
    public Vector3 Deacceleration = new Vector3(0, 9, 0);

    void Awake()
    {
        characterController = GameObject.Find("Fox").GetComponent<CharacterController>();
    }

    public void LaunchPlayer()
    {
        _velocity += Force;
    }

    void Update()
    {
        _velocity.x /= 1 + Deacceleration.x * Time.deltaTime;
        _velocity.y /= 1 + Deacceleration.y * Time.deltaTime;
        _velocity.z /= 1 + Deacceleration.z * Time.deltaTime;

        characterController.Move(_velocity * Time.deltaTime);
    }
}
