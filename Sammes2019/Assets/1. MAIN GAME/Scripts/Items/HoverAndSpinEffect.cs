﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverAndSpinEffect : MonoBehaviour
{
    [SerializeField]
    public Vector3 centerPos;
    public Vector3 offset;
    public float moveSpeed;
    public float amplitude = 1.0f;
    public float period = 1.0f;
    float degrees;

    // Update is called once per frame
    void Update()
    {
        float deltaTime = Time.deltaTime;

        // Update degrees
        float degreesPerSecond = 360.0f / period;
        degrees = Mathf.Repeat(degrees + (deltaTime * degreesPerSecond), 360.0f);
        float radians = degrees * Mathf.Deg2Rad;

        // Offset by sin wave
        offset = new Vector3(0.0f, amplitude * Mathf.Sin(radians), 0.0f);
        transform.localPosition = transform.parent.localPosition + offset;
        transform.Rotate(new Vector3(0, 0.2f, 0), Space.Self);
    }
}
