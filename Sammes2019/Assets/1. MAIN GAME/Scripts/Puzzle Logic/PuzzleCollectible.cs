﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Collectible Logic - When the player is close, get "picked up" and despawn
// If the puzzle is over, delete itself
public class PuzzleCollectible : MonoBehaviour
{
    public GameObject parentPuzzle;
    public string patternPartSign = "placeholder_STRING";

    void OnTriggerEnter(Collider other)
    {
        //If it's a normal collectible puzzle, destroy it and decrease objectives
        if (other.gameObject.CompareTag("Player") && (int)parentPuzzle.GetComponent<PuzzleCore>().puzzle == 0)
        {
            parentPuzzle.GetComponent<PuzzleCore>().objectives--;
            parentPuzzle.GetComponent<PuzzleCore>().progress++;
            //parentPuzzle.GetComponent<PuzzleCore>().objectiveDescription = parentPuzzle.GetComponent<PuzzleCore>().progress + " / " + parentPuzzle.GetComponent<PuzzleCore>().goal;
            //parentPuzzle.GetComponent<PuzzleCore>().uiDescRef.text = parentPuzzle.GetComponent<PuzzleCore>().objectiveDescription;
            Destroy(gameObject);
        }

        //If it's a hybrid puzzle, "collect it" and transport it to one of the pads
        if (other.gameObject.CompareTag("Player") && (int)parentPuzzle.GetComponent<PuzzleCore>().puzzle == 3)
        {
            if(parentPuzzle.GetComponent<PuzzleCore>().pieceCollected == false)
            {
                parentPuzzle.GetComponent<PuzzleCore>().CodeCollector(this.gameObject);
                parentPuzzle.GetComponent<PuzzleCore>().objectives--;
                this.transform.SetParent(GameObject.Find("HeadTarget").transform);
                this.transform.localPosition = new Vector3(0, 0, 0);
                Destroy(this.GetComponent<Rigidbody>());
                Destroy(this.GetComponent<BoxCollider>());
                this.GetComponent<HoverAndSpinEffect>().enabled = true;
            }

            else
            {
                Debug.LogError("You already have a piece collected! Turn it in before you collect a new one");
            }
        }
    }

    void Update()
    {
        if(parentPuzzle == null)
        {
            Destroy(gameObject);
        }
    }
}