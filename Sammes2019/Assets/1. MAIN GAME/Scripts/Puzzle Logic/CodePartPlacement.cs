﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodePartPlacement : MonoBehaviour
{
    public GameObject parentPuzzle;
    public string sign;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && (int)parentPuzzle.GetComponent<PuzzleCore>().puzzle == 2)
        {
            parentPuzzle.GetComponent<PuzzleCore>().CodeAssembler(gameObject);
        }

        //If it's a hybrid puzzle, "collect it" and transport it to one of the pads
        if (other.gameObject.CompareTag("Player") && (int)parentPuzzle.GetComponent<PuzzleCore>().puzzle == 3)
        {
            if (parentPuzzle.GetComponent<PuzzleCore>().pieceCollected)
            {
                parentPuzzle.GetComponent<PuzzleCore>().CodeDumper(gameObject);
            }

            else if(parentPuzzle.GetComponent<PuzzleCore>().patternsRestored == false)
            {
                Debug.LogError("You don't have any part to repair this pad");
            }
        }
    }

    void Update()
    {
        if (parentPuzzle == null)
        {
            Destroy(gameObject);
        }
    }

    //Set's this as the new CallingPad object
    public void SetCallingPad(GameObject pad)
    {
        if(GameObject.FindGameObjectWithTag("CallingPad"))
        {
            GameObject lastPad = GameObject.FindGameObjectWithTag("CallingPad");
            lastPad.tag = "Untagged";
        }

        pad.tag = "CallingPad";
    }
}