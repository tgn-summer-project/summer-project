﻿using Gamekit3D;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PuzzleCore : MonoBehaviour
{
    public enum PuzzleType : int { Collectible, Arena, Pattern, HybridCP }
    public enum TimerSetting : int { Off, On }

    private GameMaster gmRef;

    [SerializeField]
    [Header("Timer - Settings")]
    public PuzzleType puzzle;
    public TimerSetting timer;
    public float timerTime;

    [Header("Puzzle - Settings (General)")]
    public int objectives;
    public GameObject[] spawnZones;
    public Gamekit3D.GameCommands.SendGameCommand[] eventOnWin;
    public Gamekit3D.GameCommands.SendGameCommand[] eventOnFail;
    public Transform puzzleArea;
    public bool victory;
    public bool defeat;

    [Header("Collectible Puzzle - Settings")]
    public GameObject objectiveRef;
    private Component[] collectibleTexts;
    private GameObject instanceRef;

    [HideInInspector]
    private GameObject rndZone;
    private Vector3 onNavSpawnPos;

    [Header("Arena Puzzle - Settings")]
    public GameObject boss;
    public string bossName;
    public GameObject bossCanvas;
    public TMPro.TextMeshProUGUI bossUIName;
    public SimpleHealthBar bossUIHealth;
    public bool bossDefeated;
    public bool arenaActive;
    public GameObject[] arenaBarricades;

    [Header("Code Pattern Puzzle - Settings")]
    public int codeLength;
    public string[] codes;
    public string[] codePartsSigns;
    public GameObject[] codeFrameSignParts;
    public GameObject[] codePartOrbs;
    private string[] orbCodeAssignerSigns;
    private GameObject[] orbCodeAssignerOrbs;
    public int assignerIndex;
    public string awnserCode;
    private int arrCap;
    public GameObject callingOrb;
    public GameObject codeInputShowcase;
    public GameObject[] codeFrames;
    public Transform[] codeOrbLocations;
    public string codeAttempt;
    private List<GameObject> signsWritten = new List<GameObject>();

    [Header("Hybrid Puzzle - Settings")]
    public GameObject currentPartAccuiered;
    public bool pieceCollected;
    public bool patternsRestored;

    [HideInInspector]
    public int goal;
    public int progress;

    public void Start()
    {
        //Set the GM ref
        gmRef = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        pieceCollected = false;
        patternsRestored = false;

        //Check if any key parameters are unassigned
        if (objectives == 0 && objectiveRef == null && (int)puzzle == 0)
        {
            Debug.LogError("PuzzleObjectiveException: Make sure all COLLECTIBLE PUZZLE related parts are correctly filled in!");
        }

        if (codeLength == 0 && (int)puzzle == 2 || codePartsSigns.Length != 4 && (int)puzzle == 2 || codeFrames.Length != 4 && (int)puzzle == 2 /*|| codeOrbRef == null && (int)puzzle == 2*/)
        {
            Debug.LogError("PuzzleObjectiveException: Make sure all CODE PUZZLE related parts are correctly filled in!");
            Debug.Log(codeLength);
            Debug.Log(codePartsSigns.Length);
            Debug.Log(codeFrames.Length);
            //Debug.Log(codeOrbRef);
        }

        if (objectives == 0 && (int)puzzle == 1)
        {
            Debug.LogError("PuzzleObjectiveException: Make sure all ARENA PUZZLE related parts are correctly filled in!");
        }

        if (eventOnWin.Length == 0)
        {
            Debug.LogError("PuzzleVictoryEventException: No win events assigned!");
        }

        if (eventOnFail.Length == 0 && (int)timer == 1)
        {
            Debug.LogError("PuzzleFailEventException: No fail events assigned!");
        }

        //Check what kind of puzzle it is and execute after that standard
        switch ((int)puzzle)
        {
            //Collectible Puzzle
            case 0:
                for (int i = 0; i < objectives; i++)
                {
                    Vector3 rndPosWithin;
                    bool allowSpawnOfCollectible = false;

                    while(!allowSpawnOfCollectible)
                    {
                        rndZone = spawnZones[UnityEngine.Random.Range(0, spawnZones.Length)];
                        
                        if(rndZone.GetComponent<PuzzleCollectibleSpawnZone>().currentStock == rndZone.GetComponent<PuzzleCollectibleSpawnZone>().maxCap)
                        {
                            Debug.Log(rndZone.name + " has reached it's max capacity, looking for another zone");
                        }

                        else
                        {
                            Debug.Log("Object spawned within zone: " + rndZone.name);
                            allowSpawnOfCollectible = true;
                        }
                    }

                    rndPosWithin = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
                    rndPosWithin = rndZone.transform.TransformPoint(rndPosWithin * .5f);
                    rndZone.GetComponent<PuzzleCollectibleSpawnZone>().currentStock++;

                    NavMeshHit posHit;
                    if (NavMesh.SamplePosition(rndPosWithin, out posHit, 10.0f, NavMesh.AllAreas))
                    {
                        onNavSpawnPos = posHit.position;
                    }

                    instanceRef = Instantiate(objectiveRef, onNavSpawnPos, transform.rotation);
                    instanceRef.GetComponent<PuzzleCollectible>().parentPuzzle = gameObject;
                }

                break;

            //Arena Puzzle
            case 1:
                bossDefeated = false;
                boss.SetActive(false);
                bossCanvas.SetActive(false);
                bossUIName.text = bossName;

                for (int i = 0; i < arenaBarricades.Length; i++)
                {
                    arenaBarricades[i].SetActive(false);
                }

                break;

            //Pattern Puzzle
            case 2:
                //Generates the codes for the puzzle using the given signs
                arrCap = codePartsSigns.Length;
                codes = new string[4];

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < codeLength; j++)
                    {
                        codes[i] += codePartsSigns[UnityEngine.Random.Range(0, arrCap)];
                    }
                }

                //Set the text of the code hints in the world
                for (int k = 0; k < codes.Length; k++)
                {
                    //How long is this code?
                    for (int i = 0; i < codes[k].Length; i++)
                    {
                        for (int j = 0; j < codePartsSigns.Length; j++)
                        {
                            /*
                            * TODO:
                            * 1) Get CodeFrame and it's sign positions
                            * 2) See what sign is on the current pos in this code, then assign the corresponding element sign to the pos in the code hint we are on
                            * 3) Make sure everything works fine
                            */

                            if (codes[k][i].ToString() == codePartsSigns[j])
                            {
                                Debug.Log("CODE #" +k+ " - SIGN #" +i+ ": " +codes[k][i].ToString());
                                Debug.Log("GAME OBJECT NAME: " +codeFrames[k].transform.Find("Pos" + i));
                                GameObject signSpawn = Instantiate(codeFrameSignParts[j], codeFrames[k].transform, false);
                                signSpawn.transform.localPosition = codeFrames[k].transform.Find("Pos" + i).transform.localPosition;
                                signSpawn.transform.localScale = codeFrames[k].transform.Find("Pos" + i).transform.localScale;
                                break;
                            }
                        }
                    }
                }

                //Choos one of the random codes as the correct awnser
                awnserCode = codes[UnityEngine.Random.Range(0, codes.Length)];

                //Set the size of Code Assigner before feeding values
                orbCodeAssignerSigns = new string[codePartsSigns.Length];
                orbCodeAssignerOrbs = new GameObject[codePartOrbs.Length];

                for (int l = 0; l < codePartsSigns.Length; l++)
                {
                    orbCodeAssignerSigns[l] = codePartsSigns[l];
                    orbCodeAssignerOrbs[l] = codePartOrbs[l];
                }

                //Spawn Orbs with signs to enter code
                for (int m = 0; m < codePartsSigns.Length; m++)
                {
                    assignerIndex = Array.IndexOf(orbCodeAssignerSigns, orbCodeAssignerSigns[UnityEngine.Random.Range(0, orbCodeAssignerSigns.Length)]);

                    //Create the pad
                    instanceRef = Instantiate(orbCodeAssignerOrbs[assignerIndex], codeOrbLocations[m].position, codeOrbLocations[m].rotation);
                    instanceRef.GetComponent<CodePartPlacement>().parentPuzzle = gameObject;

                    //Randomize a sign to the instantiated pad, then delete it from the ref array
                    instanceRef.GetComponent<CodePartPlacement>().sign = orbCodeAssignerSigns[assignerIndex].ToString();
                    orbCodeAssignerSigns[assignerIndex] = orbCodeAssignerSigns[orbCodeAssignerSigns.Length - 1];
                    orbCodeAssignerOrbs[assignerIndex] = orbCodeAssignerOrbs[orbCodeAssignerOrbs.Length - 1];
                    Array.Resize(ref orbCodeAssignerSigns, orbCodeAssignerSigns.Length - 1);
                    Array.Resize(ref orbCodeAssignerOrbs, orbCodeAssignerOrbs.Length - 1);
                }

                break;

            //Hybrid Puzzle
            case 3:
                //Generates the codes for the puzzle using the given signs

                arrCap = codePartsSigns.Length;
                codes = new string[4];

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < codeLength; j++)
                    {
                        codes[i] += codePartsSigns[UnityEngine.Random.Range(0, arrCap)];
                    }
                }

                //Set the text of the code hints in the world
                for (int k = 0; k < codes.Length; k++)
                {
                    codeFrames[k].GetComponent<TMPro.TextMeshPro>().text = codes[k];
                }

                //Choos one of the random codes as the correct awnser
                awnserCode = codes[UnityEngine.Random.Range(0, codes.Length)];

                //Set the size of Code Assigner before feeding values
                orbCodeAssignerSigns = new string[codePartsSigns.Length];

                for (int l = 0; l < codePartsSigns.Length; l++)
                {
                    orbCodeAssignerSigns[l] = codePartsSigns[l];
                }

                //Spawn collectibles with signs to enter code
                for (int m = 0; m < codePartsSigns.Length; m++)
                {
                    Vector3 rndPosWithin;
                    bool allowSpawnOfCollectible = false;

                    assignerIndex = Array.IndexOf(orbCodeAssignerSigns, orbCodeAssignerSigns[UnityEngine.Random.Range(0, orbCodeAssignerSigns.Length)]);

                    while (!allowSpawnOfCollectible)
                    {
                        rndZone = spawnZones[UnityEngine.Random.Range(0, spawnZones.Length)];

                        if (rndZone.GetComponent<PuzzleCollectibleSpawnZone>().currentStock == rndZone.GetComponent<PuzzleCollectibleSpawnZone>().maxCap)
                        {
                            Debug.Log(rndZone.name + " has reached it's max capacity, looking for another zone");
                        }

                        else
                        {
                            Debug.Log("Object spawned within zone: " + rndZone.name);
                            allowSpawnOfCollectible = true;
                        }
                    }

                    rndPosWithin = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
                    rndPosWithin = rndZone.transform.TransformPoint(rndPosWithin * .5f);
                    rndZone.GetComponent<PuzzleCollectibleSpawnZone>().currentStock++;

                    NavMeshHit posHit;
                    if(NavMesh.SamplePosition(rndPosWithin, out posHit, 10.0f, NavMesh.AllAreas))
                    {
                        onNavSpawnPos = posHit.position;
                    }

                    //Create the collectible and set it's code part value
                    instanceRef = Instantiate(objectiveRef, onNavSpawnPos, transform.rotation);
                    instanceRef.GetComponent<PuzzleCollectible>().parentPuzzle = gameObject;
                    instanceRef.GetComponent<PuzzleCollectible>().patternPartSign = orbCodeAssignerSigns[assignerIndex].ToString();

                    collectibleTexts = instanceRef.GetComponentsInChildren<TMPro.TextMeshPro>();

                    foreach(TMPro.TextMeshPro currentText in collectibleTexts)
                    {
                        currentText.text = orbCodeAssignerSigns[assignerIndex].ToString();
                    }

                    //Delete that sign from the array so no duplicates can appear
                    orbCodeAssignerSigns[assignerIndex] = orbCodeAssignerSigns[orbCodeAssignerSigns.Length - 1];
                    Array.Resize(ref orbCodeAssignerSigns, orbCodeAssignerSigns.Length - 1);

                    //Once the collectible is created, create the pad at the correct positions
                    //instanceRef = Instantiate(codeOrbRef, codeOrbLocations[m].position, codeOrbLocations[m].rotation);
                    instanceRef.GetComponent<CodePartPlacement>().parentPuzzle = gameObject;
                }

                break;
        }
    }

    //Control the code
    public IEnumerator ControlCode(string codeTrial)
    {
        yield return new WaitForSeconds(3);

        if (codeAttempt == awnserCode)
        {
            victory = true;
            //codeInputShowcase.GetComponent<TMPro.TextMeshPro>().text = "(^w^)";
            objectives = 0;
        }

        else
        {
            //codeInputShowcase.GetComponent<TMPro.TextMeshPro>().text = "(-.-)";
            for (int i = 0; i < signsWritten.Count; i++)
            {
                Destroy(signsWritten[i]);
            }

            signsWritten.Clear();
            codeAttempt = "";
        }
    }

    //Pick up the code and keep it in an "inventory"
    public void CodeCollector(GameObject collectedCodePiece)
    {
        currentPartAccuiered = collectedCodePiece;
        pieceCollected = true;
    }

    //Assign the collected code piece to the pad
    public void CodeDumper(GameObject pad)
    {
        callingOrb = pad;

        if(callingOrb.GetComponentInChildren<TMPro.TextMeshPro>().text != "")
        {
            Debug.LogError("This pad already has a code assigned to it");
        }

        else
        {
            callingOrb.GetComponentInChildren<TMPro.TextMeshPro>().text = currentPartAccuiered.GetComponentInChildren<TMPro.TextMeshPro>().text;
            Debug.Log("The code piece has been assigned to a code pad");
            //objectiveDescription = "Repair The Puzzle : " +objectives+ " part(s) remains";
            //uiDescRef.text = objectiveDescription;
            Destroy(currentPartAccuiered);
            pieceCollected = false;

            if (objectives == 0)
            {
                //objectiveDescription = "Solve the code";
                //uiDescRef.text = objectiveDescription;
                patternsRestored = true;
            }
        }
    }

    //Take input from pressure pads and add them to the code string
    public void CodeAssembler(GameObject callOrb)
    {
        if(((int)puzzle == 3 && patternsRestored == true) || ((int)puzzle == 2 && codeAttempt.Length < codeLength))
        {
            callingOrb = callOrb;
            Debug.Log(callingOrb.name);
            string codeInput = callingOrb.GetComponent<CodePartPlacement>().sign;
            codeAttempt += codeInput;

            for (int i = 0; i < codePartsSigns.Length; i++)
            {
                if (codeInput == codePartsSigns[i])
                {
                    GameObject signSpawn = Instantiate(codeFrameSignParts[i], codeInputShowcase.transform, false);
                    signSpawn.transform.localPosition = codeInputShowcase.transform.Find("Pos" + (codeAttempt.Length - 1)).transform.localPosition;
                    signSpawn.transform.localScale = codeInputShowcase.transform.Find("Pos" + (codeAttempt.Length - 1)).transform.localScale;
                    signsWritten.Add(signSpawn);
                    break;
                }
            }

            //codeInputShowcase.GetComponent<TMPro.TextMeshPro>().text = codeAttempt;

            if (codeAttempt.Length == codeLength)
            {
                Debug.Log("The code is ready for check!");
                StartCoroutine(ControlCode(codeAttempt));
            }
        }
    }

    //Only used for Arena puzzles
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && (int)puzzle == 1)
        {
            boss.SetActive(true);
            bossCanvas.SetActive(true);
            gmRef.SwitchMusicMode();

            Debug.Log("BOSS START HP:" + boss.GetComponent<Damageable>().currentHitPoints);
            Debug.Log("BOSS CURRENT HP:" +boss.GetComponent<EnemyStatController>().Health.BaseValue);

            for (int i = 0; i < arenaBarricades.Length; i++)
            {
                arenaBarricades[i].SetActive(true);
            }

            this.GetComponent<BoxCollider>().enabled = false;

            arenaActive = true;
        }
    }

    void Update()
    {
        switch((int) puzzle)
        {
            //Collectible Puzzle
            case 0:

                //If the timer is activated and run out of time, PUNISH
                if (timer == TimerSetting.On && objectives > 0)
                {
                    timerTime -= Time.deltaTime;

                    if (timerTime <= 0 && objectives > 0)
                    {
                        defeat = true;
                        timer = TimerSetting.Off;
                    }
                }

                //If you complete the objectives before you fulfill any fail conditions, you win
                else
                {
                    if (objectives == 0 && !defeat)
                    {
                        victory = true;
                        Debug.Log("VICTORY! PUZZLE COMPLETED");
                    }
                }

                //Activates all victory events upon successful puzzle
                if (victory)
                {
                    for (int k = 0; k < eventOnWin.Length; k++)
                    {
                        eventOnWin[k].Send();
                    }

                    Destroy(gameObject);
                }

                //Activates all fail events upon unsuccessful puzzle, if it's possible to fail
                else if (defeat)
                {
                    for (int l = 0; l < eventOnFail.Length; l++)
                    {
                        eventOnFail[l].Send();
                    }

                    Destroy(gameObject);
                }

                break;

            //Arena Puzzle
            case 1:
                //If the timer is activated and run out of time, PUNISH
                if (timer == TimerSetting.On && arenaActive)
                {
                    timerTime -= Time.deltaTime;

                    if (timerTime <= 0 && victory != true)
                    {
                        defeat = true;
                        timer = TimerSetting.Off;
                    }
                }

                //If the boss id defeated
                if (!boss && arenaActive)
                {
                    arenaActive = false;
                    victory = true;
                    Destroy(bossCanvas);
                }

                //Activates all victory events upon successful puzzle
                if (victory)
                {
                    timer = TimerSetting.Off;

                    Debug.Log("ARENA COMPLETED - BOSS DEFEATED");

                    for (int k = 0; k < eventOnWin.Length; k++)
                    {
                        eventOnWin[k].Send();
                        gmRef.SwitchMusicMode();
                    }

                    for (int i = 0; i < arenaBarricades.Length; i++)
                    {
                        arenaBarricades[i].SetActive(false);
                    }

                    Destroy(gameObject);
                }

                //Activates all fail events upon unsuccessful puzzle, if it's possible to fail
                else if (defeat)
                {
                    for (int l = 0; l < eventOnFail.Length; l++)
                    {
                        eventOnFail[l].Send();
                    }

                    Destroy(gameObject);
                }

                break;

            //Pattern Puzzle
            case 2:

                //If the timer is activated and run out of time, PUNISH
                if (timer == TimerSetting.On)
                {
                    timerTime -= Time.deltaTime;

                    if (timerTime <= 0 && victory != true)
                    {
                        defeat = true;
                        timer = TimerSetting.Off;
                    }
                }

                //Activates all victory events upon successful puzzle
                if (victory)
                {
                    for (int k = 0; k < eventOnWin.Length; k++)
                    {
                        eventOnWin[k].Send();
                    }

                    Destroy(gameObject);
                }


                //Activates all fail events upon unsuccessful puzzle, if it's possible to fail
                else if (defeat)
                {
                    for (int l = 0; l < eventOnFail.Length; l++)
                    {
                        eventOnFail[l].Send();
                    }

                    Destroy(gameObject);
                }

                break;

            //Hybrid Puzzle
            case 3:

                //If the timer is activated and run out of time, PUNISH
                if (timer == TimerSetting.On)
                {
                    timerTime -= Time.deltaTime;

                    if (timerTime <= 0 && victory != true)
                    {
                        defeat = true;
                        timer = TimerSetting.Off;
                    }
                }

                //Activates all victory events upon successful puzzle
                if (victory)
                {
                    for (int k = 0; k < eventOnWin.Length; k++)
                    {
                        eventOnWin[k].Send();
                    }

                    Destroy(gameObject);
                }


                //Activates all fail events upon unsuccessful puzzle, if it's possible to fail
                else if (defeat)
                {
                    for (int l = 0; l < eventOnFail.Length; l++)
                    {
                        eventOnFail[l].Send();
                    }

                    Destroy(gameObject);
                }

                break;
        }
    }
}