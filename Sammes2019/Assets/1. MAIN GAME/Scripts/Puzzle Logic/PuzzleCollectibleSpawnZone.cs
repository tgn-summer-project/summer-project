﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCollectibleSpawnZone : MonoBehaviour
{
    [SerializeField]
    public int maxCap;

    [HideInInspector]
    public int currentStock;
}