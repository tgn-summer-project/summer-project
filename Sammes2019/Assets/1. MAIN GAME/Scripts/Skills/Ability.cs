﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public interface IAbility 
{
    UnityEvent OnActivate { get; set; }

    float coolDownDuration { get; set; }
    float nextReadyTime { get; set; }
    float coolDownTimeLeft { get; set; }
    void Activate();
}