﻿using System.Collections;
using System.Linq;
using Gamekit3D;

using UnityEngine;
using UnityEngine.UI;

public class ElementalMight : MonoBehaviour
{
    public Image SkillImage;
    private Image cooldownImage;
    private TMPro.TextMeshProUGUI cooldownText;

    public PlayerStats PlayerStats;
    public Projectile Projectile;

    public float ActiveDuration = 3;
    public float CoolDownDuration = 20;

    private Spit spitComp;

    private bool isOnCooldown;
    private float duration = 3;
    private float coolDownDuration = 10;
    private float nextReadyTime;
    private float coolDownTimeLeft; // use for display text

    void Awake()
    {
        spitComp = Projectile.GetComponent<Spit>();
        cooldownImage = SkillImage.GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "Cooldown");
        cooldownText = SkillImage.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        cooldownText.text = "";
    }

    public void Activate()
    {   
        if (Time.time <= nextReadyTime)
        {
            return;
        }

        PlayerStats.IsBurstActive = true;
        isOnCooldown = true;
        nextReadyTime = CoolDownDuration + Time.time;
        coolDownTimeLeft = CoolDownDuration;
        cooldownImage.fillAmount = 1;

        spitComp.projectileSpeed += 25;
        PlayerStats.AttackSpeed.AddMod(new StatModifier(1.5f, ModType.PercentAdd, this));
        StartCoroutine(Disable());
    }

    IEnumerator Disable()
    {
        yield return new WaitForSeconds(ActiveDuration);
        PlayerStats.IsBurstActive = false;
        spitComp.projectileSpeed -= 25;
        PlayerStats.AttackSpeed.RemoveAllModsFromSource(this);
    }

    void Update()
    {
        if (coolDownTimeLeft > 0)
            coolDownTimeLeft -= Time.deltaTime;

        if (isOnCooldown)
        {
            cooldownImage.fillAmount -= Mathf.Max(0, cooldownImage.fillAmount / coolDownTimeLeft * Time.deltaTime);
            cooldownText.text = coolDownTimeLeft >= 1 ? $"{(int)coolDownTimeLeft}" : $"{coolDownTimeLeft:0.#}";


            if (cooldownImage.fillAmount <= 0)
            {
                cooldownImage.fillAmount = 0;
                isOnCooldown = false;
                cooldownText.text = "";
            }
        }
    }
}
