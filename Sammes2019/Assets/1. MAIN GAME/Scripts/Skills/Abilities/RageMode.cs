﻿using System.Collections;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

public class RageMode : MonoBehaviour
{
    public Image SkillImage;
    private Image cooldownImage;
    private TMPro.TextMeshProUGUI cooldownText;

    public float SkillDuration = 5;
    public float CoolDownDuration = 20;

    public PlayerStats PlayerStats;

    private bool isOnCooldown;
 
    private float nextReadyTime;
    private float coolDownTimeLeft; // use for display text

    void Awake()
    {
        cooldownImage = SkillImage.GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "Cooldown");
        cooldownText = SkillImage.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        cooldownText.text = "";
    }

    public void Activate()
    {
        if (Time.time <= nextReadyTime)
        {
            return;
        }

        isOnCooldown = true;

        nextReadyTime = CoolDownDuration + SkillDuration + Time.time;
        coolDownTimeLeft = CoolDownDuration + SkillDuration;

        cooldownImage.fillAmount = 1;

        PlayerStats.MaxHealth.AddMod(new StatModifier(0.35f, ModType.PercentAdd, this));
        PlayerStats.RunningSpeed.AddMod(new StatModifier(0.35f, ModType.PercentAdd, this));
        PlayerStats.AttackSpeed.AddMod(new StatModifier(0.05f, ModType.PercentAdd, this));

        StartCoroutine(Disable());
    }
    IEnumerator Disable()
    {
        yield return new WaitForSeconds(SkillDuration);

        PlayerStats.AttackSpeed.RemoveAllModsFromSource(this);
        PlayerStats.MaxHealth.RemoveAllModsFromSource(this);
        PlayerStats.RunningSpeed.RemoveAllModsFromSource(this);
        PlayerStats.AttackSpeed.RemoveAllModsFromSource(this);
    }

    void Update()
    {
        if (coolDownTimeLeft > 0)
            coolDownTimeLeft -= Time.deltaTime;

        if (isOnCooldown)
        {
            cooldownImage.fillAmount -= Mathf.Max(0, cooldownImage.fillAmount / coolDownTimeLeft * Time.deltaTime); 
            cooldownText.text = coolDownTimeLeft < 1 ? $"{coolDownTimeLeft:0.#}" : $"{(int)coolDownTimeLeft}";

            if (cooldownImage.fillAmount <= 0)
            {
                cooldownImage.fillAmount = 0;
                isOnCooldown = false;
                cooldownText.text = "";
            }
        }
    }
}
