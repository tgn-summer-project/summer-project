﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Push : MonoBehaviour
{
    public Image SkillImage;
    private Image CooldownImage;
    private TMPro.TextMeshProUGUI CooldownText;

    public GameObject DisortionEffect;
    public float CoolDownDuration = 6;

    private bool isOnCooldown;
    private float nextReadyTime;
    private float coolDownTimeLeft; // use for display text

    void Awake()
    {
        CooldownImage = SkillImage.GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "Cooldown");
        CooldownText = SkillImage.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        CooldownText.text = "";
    }

    public void Activate()
    {
        if (Time.time <= nextReadyTime)
        {
            return;
        }

        isOnCooldown = true;
        nextReadyTime = CoolDownDuration + Time.time;
        coolDownTimeLeft = CoolDownDuration;
        CooldownImage.fillAmount = 1;

        var clone = Instantiate(DisortionEffect, transform.position, Quaternion.identity);
        Destroy(clone, 1);
    }

    void Update()
    {
        if (coolDownTimeLeft > 0)
            coolDownTimeLeft -= Time.deltaTime;

        if (isOnCooldown)
        {
            CooldownImage.fillAmount -= Mathf.Max(0, CooldownImage.fillAmount / coolDownTimeLeft * Time.deltaTime);
            CooldownText.text = coolDownTimeLeft >= 1 ? $"{(int)coolDownTimeLeft}" : $"{coolDownTimeLeft:0.#}";

            if (CooldownImage.fillAmount <= 0)
            {
                CooldownImage.fillAmount = 0;
                isOnCooldown = false;
                CooldownText.text = "";
            }
        }
    }
}
