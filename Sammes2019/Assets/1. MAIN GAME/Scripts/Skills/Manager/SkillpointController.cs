﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Gamekit3D;
using UnityEngine;
using UnityEngine.UI;

public class SkillpointController : MonoBehaviour
{
    public GameObject SkillsystemHUD;

    public bool isToggled;

    public Text AbilityPoints;
    public Text CritChance;
    public Text AtkSpeed;
    public Text MaxHP;
    public Text MeleeDmg;
    public Text RunSpeed;
    public Text RangeDmg;

    private PlayerStats PlayerStats;
    private PlayerController pC;
    private Cinemachine.CinemachineFreeLook keyAndMouseRot;
    private Cinemachine.CinemachineFreeLook controllerRot;
    private GameObject abilitiesCanvas;
    public Button[] skillButtons;

    private void Awake()
    {
        PlayerStats = GameObject.Find("Fox").GetComponent<PlayerStats>();
        pC = GameObject.Find("Fox").GetComponent<PlayerController>();
        keyAndMouseRot = GameObject.Find("KeyboardAndMouseFreeLookRig").GetComponent<CinemachineFreeLook>();
        controllerRot = GameObject.Find("ControllerFreeLookRig").GetComponent<CinemachineFreeLook>();
        abilitiesCanvas = GameObject.Find("Abilities");
    }

    private void Update()
    {
        if (isToggled)
        {
            RefreshText();
        }
    }

    private void RefreshText()
    {
        AbilityPoints.text = $"Ability Points {PlayerStats.AvailableSkillPoints}";
        CritChance.text = $"Critical Chance {Mathf.Min(PlayerStats.Critchance.BaseValue, 100):0.#}%";
        AtkSpeed.text = $"Attack Speed {PlayerStats.AttackSpeed.BaseValue:0.#}%";
        MaxHP.text = $"Max HP {PlayerStats.MaxHealth.BaseValue:0.#}";
        MeleeDmg.text = $"Melee DMG {PlayerStats.MeleeDamage.BaseValue:0.#}";
        RunSpeed.text = $"Run Speed x {PlayerStats.RunningSpeed.BaseValue:0.#}";
        RangeDmg.text = $"Ranged DMG {PlayerStats.RangedDamage.BaseValue:0.#}";
    }

    public void Enable()
    {
        isToggled = true;
        Time.timeScale = 0.0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        pC.enabled = false;
        keyAndMouseRot.enabled = false;
        controllerRot.enabled = false;
        SkillsystemHUD.SetActive(true);
        abilitiesCanvas.SetActive(false);
    }

    public void Disable()
    {
        isToggled = false;
        Time.timeScale = 1.0f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        pC.enabled = true;
        keyAndMouseRot.enabled = true;
        controllerRot.enabled = true;
        SkillsystemHUD.SetActive(false);
        abilitiesCanvas.SetActive(true);
    }
}
