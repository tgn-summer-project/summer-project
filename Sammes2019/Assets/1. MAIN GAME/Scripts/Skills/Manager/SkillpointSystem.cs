﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillpointSystem : MonoBehaviour
{
    private PlayerStats PlayerStats;
    private int strengthLimit = 14;
    private int staminaLimit = 10;
    private int dexterityLimit = 5;
    private int intelligenceLimit = 10;
    private GameObject skillSystemRef;

    void Awake()
    {
        PlayerStats = GameObject.Find("Fox").GetComponent<PlayerStats>();

        PlayerStats.Strength.OnAddedMod.AddListener(delegate
        {
            PlayerStats.MeleeDamage.AddMod(new StatModifier(0.3f, ModType.PercentAdd, this));
        });

        PlayerStats.Stamina.OnAddedMod.AddListener(delegate
        {
            PlayerStats.MaxHealth.AddMod(new StatModifier(0.1f, ModType.PercentAdd, this));
            PlayerStats.RunningSpeed.AddMod(new StatModifier(0.033f, ModType.PercentAdd, this));
        });

        PlayerStats.Dexterity.OnAddedMod.AddListener(delegate
        {
            PlayerStats.Critchance.AddMod(new StatModifier(5f, ModType.Flat, this));
        });

        PlayerStats.Intelligence.OnAddedMod.AddListener(delegate
        {
            PlayerStats.RangedDamage.AddMod(new StatModifier(0.25f, ModType.PercentAdd, this));
            PlayerStats.AttackSpeed.AddMod(new StatModifier(1.2f, ModType.Flat, this));
            PlayerStats.AttackSpeed.AddMod(new StatModifier(0.02f, ModType.PercentAdd, this));
        });

        /*PlayerStats.MeleeDamage.BaseValue = PlayerStats.MeleeDamage.Value;
        PlayerStats.AttackSpeed.BaseValue = PlayerStats.AttackSpeed.Value;
        PlayerStats.RangedDamage.BaseValue = PlayerStats.RangedDamage.Value;
        PlayerStats.Critchance.BaseValue = PlayerStats.Critchance.Value;
        PlayerStats.RunningSpeed.BaseValue = PlayerStats.RunningSpeed.Value;
        PlayerStats.MaxHealth.BaseValue = PlayerStats.MaxHealth.Value;*/
    }

    public void AddStamina()
    {
        if (staminaLimit > 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            staminaLimit--;
            AddAttribute(PlayerStats.Stamina, new StatModifier(1, ModType.Flat, this));
            PlayerStats.RunningSpeed.BaseValue = PlayerStats.RunningSpeed.Value;
            PlayerStats.MaxHealth.BaseValue = PlayerStats.MaxHealth.Value;
        }

        else if (staminaLimit == 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            Debug.Log("You reached the Max Level for Stamina");
        }

        else if(staminaLimit > 0 && PlayerStats.AvailableSkillPoints == 0)
        {
            Debug.Log("You can't upgrade right now! No skill points avalible");
        }
    }

    public void AddStrength()
    {
        if (strengthLimit > 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            strengthLimit--;
            AddAttribute(PlayerStats.Strength, new StatModifier(1, ModType.Flat, this));
            PlayerStats.MeleeDamage.BaseValue = PlayerStats.MeleeDamage.Value;
        }

        else if (strengthLimit == 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            Debug.Log("You reached the Max Level for Strength");
        }

        else if (strengthLimit > 0 && PlayerStats.AvailableSkillPoints == 0)
        {
            Debug.Log("You can't upgrade right now! No skill points avalible");
        }
    }
    public void AddDexterity()
    {
        if (dexterityLimit > 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            dexterityLimit--;
            AddAttribute(PlayerStats.Dexterity, new StatModifier(1, ModType.Flat, this));
            PlayerStats.Critchance.BaseValue = PlayerStats.Critchance.Value;
        }

        else if (dexterityLimit == 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            Debug.Log("You reached the Max Level for Dexterity");
        }

        else if (dexterityLimit > 0 && PlayerStats.AvailableSkillPoints == 0)
        {
            Debug.Log("You can't upgrade right now! No skill points avalible");
        }
    }
    public void AddIntelligence()
    {
        if (intelligenceLimit > 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            intelligenceLimit--;
            AddAttribute(PlayerStats.Intelligence, new StatModifier(1, ModType.Flat, this));
            PlayerStats.AttackSpeed.BaseValue = PlayerStats.AttackSpeed.Value;
            PlayerStats.RangedDamage.BaseValue = PlayerStats.RangedDamage.Value;
        }

        else if (intelligenceLimit == 0 && PlayerStats.AvailableSkillPoints > 0)
        {
            Debug.Log("You reached the Max Level for Intelligence");
        }

        else if (intelligenceLimit > 0 && PlayerStats.AvailableSkillPoints == 0)
        {
            Debug.Log("You can't upgrade right now! No skill points avalible");
        }
    }

    private void AddAttribute(CharacterStat stat, StatModifier mod)
    {
        if (PlayerStats.AvailableSkillPoints > 0)
        {
            stat.AddMod(mod);
            PlayerStats.AvailableSkillPoints--;
        }
    }
}
