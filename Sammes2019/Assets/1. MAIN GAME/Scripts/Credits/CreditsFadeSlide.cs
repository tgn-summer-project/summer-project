﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CreditsFadeSlide : MonoBehaviour
{
    public GameObject creditTextSlide;
    public GameObject creditImageSlide;
    public string[] creditsTexts;
    public Image[] creditsImages;
    public RectTransform[] creditsTextsTransforms;
    public RectTransform[] creditsImagesTransforms;
    public float timePerSlide;
    private Image creditsImageDisplay;
    private TMPro.TextMeshProUGUI creditsTextDisplay;
    private bool endOfCredits;

    void Awake()
    {
        creditsTextDisplay = creditTextSlide.GetComponent<TMPro.TextMeshProUGUI>();
        creditsImageDisplay = creditImageSlide.GetComponent<Image>();
        endOfCredits = false;
    }

    void PlayCredits()
    {
        for (int i = 0; i < creditsTexts.Length; i++)
        {
            Debug.Log("Slide #" + i);

            creditsTextDisplay.text = creditsTexts[i];
            creditsImageDisplay = creditsImages[i];
            creditsTextDisplay.transform.position = creditsTextsTransforms[i].position;
            creditsImageDisplay.transform.position = creditsImagesTransforms[i].position;

            FadeIn();

            FadeOut();

            if (i == creditsTexts.Length)
            {
                endOfCredits = true;
                Debug.Log("We have reached the last slide!");
            }
        }
    }

    void FadeIn()
    {
        creditsTextDisplay.CrossFadeAlpha(1.0f, 3.0f, true);
        creditsImageDisplay.CrossFadeAlpha(1.0f, 3.0f, true);
    }

    void FadeOut()
    {
        creditsTextDisplay.CrossFadeAlpha(0.0f, 3.0f, true);
        creditsImageDisplay.CrossFadeAlpha(0.0f, 3.0f, true);
    }

    void OnFadeComplete()
    {
        StartCoroutine(DisplayCredits());
    }

    IEnumerator DisplayCredits()
    {
        yield return new WaitForSeconds(timePerSlide);
    }
}