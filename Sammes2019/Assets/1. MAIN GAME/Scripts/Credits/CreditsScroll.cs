﻿using Gamekit3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScroll : MonoBehaviour
{
    public int scrollSpeed;
    public Transform endMarker;
    public bool endOfCredits;

    // Update is called once per frame
    void Update()
    {
        if(!endOfCredits)
            transform.Translate(Vector3.up * Time.deltaTime * scrollSpeed, Space.Self);

        if (transform.position.y >= endMarker.transform.position.y)
            endOfCredits = true;
    }

    public void ResetCredits()
    {
        endOfCredits = false;
    }
}
