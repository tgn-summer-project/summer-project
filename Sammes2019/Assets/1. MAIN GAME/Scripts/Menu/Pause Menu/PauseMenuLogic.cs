﻿using System.Collections;
using System.Collections.Generic;
using Gamekit3D;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;
using Slider = UnityEngine.UI.Slider;

public class PauseMenuLogic : MonoBehaviour
{
    [Header("Pause Menu Settings")]
    public bool paused;
    private bool arenaFight;

    public PlayerController pC;
    public Cinemachine.CinemachineFreeLook keyAndMouseRot;
    public Cinemachine.CinemachineFreeLook controllerRot;
    public GameObject pauseCanvas;
    public GameObject playerUICanvas;
    public GameObject loadingScreenCanvas;
    public GameObject confirmQuitConvas;
    public GameObject bossHPCanvas;
    public Slider loadingBar;

    [Header("Settings - Sound & Music")]
    public AudioMixer mainMixer;

    public Slider masterBar;
    public Slider musicBar;
    public Slider soundFXBar;
    public Slider ambientBar;

    public float masterLevel;
    public float musicLevel;
    public float soundFXLevel;
    public float ambientLevel;

    [Header("Settings - Graphics")]
    public TMPro.TMP_Dropdown resolutionDropdown;
    public TMPro.TMP_Dropdown qualityDropdown;
    public TMPro.TMP_Dropdown screenModeDropdown;

    public int resolutionChoice;
    public int qualityChoice;
    public int screenModeChoice;

    [Header("Graphics - Current Values")]
    public string resSettings;
    public Resolution[] resolutionsAvalible;
    public List<string> resOptions = new List<string>();
    public Resolution res;
    public Resolution resolutionCurrent;
    public int qualityGrade;
    public FullScreenMode fullscreenSettings;
    public int hertz;

    // Start is called before the first frame update
    void Awake()
    {
        pC = GameObject.Find("Fox").GetComponent<PlayerController>();

        paused = false;
        Resume();

        resolutionDropdown.onValueChanged.AddListener(delegate { ResolutionValueChangeCheck(); });
        qualityDropdown.onValueChanged.AddListener(delegate { QualityValueChangeCheck(); });
        screenModeDropdown.onValueChanged.AddListener(delegate { ScreenModeValueChangeCheck(); });

        //Set resolutions setting value in UI so it matches current settings
        resolutionCurrent.width = Screen.currentResolution.width;
        resolutionCurrent.height = Screen.currentResolution.height;
        qualityGrade = QualitySettings.GetQualityLevel();
        hertz = Screen.currentResolution.refreshRate;

        mainMixer.GetFloat("MusicVolume", out musicLevel);
        musicBar.value = musicLevel;

        mainMixer.GetFloat("SFXVolume", out soundFXLevel);
        soundFXBar.value = soundFXLevel;

        mainMixer.GetFloat("AmbientVolume", out ambientLevel);
        ambientBar.value = ambientLevel;

        mainMixer.GetFloat("MasterVolume", out masterLevel);
        masterBar.value = masterLevel;

        resolutionsAvalible = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        float resXcal;
        float resYcal;
        double aspectRa;
        int hertzValidation;

        //Get a list of all resolutions avalible
        for (int i = 0; i < resolutionsAvalible.Length; i++)
        {
            resXcal = resolutionsAvalible[i].width;
            resYcal = resolutionsAvalible[i].height;
            aspectRa = resXcal / resYcal;
            hertzValidation = resolutionsAvalible[i].refreshRate;

            //Only accept 16:9 resolutions
            if (aspectRa < 1.771 || aspectRa > 1.778)
            {
                continue;
            }

            //Only accept Hz that's 59 or higher
            if (hertzValidation < 59)
            {
                continue;
            }

            string resOption = resolutionsAvalible[i].width + " x " + resolutionsAvalible[i].height + " (" + resolutionsAvalible[i].refreshRate + "Hz)";
            resOptions.Add(resOption);
        }

        //Set list to go from highest at the top to lowest at the bottom
        resOptions.Reverse();
        string currentResOption = Screen.currentResolution.width + " x " + Screen.currentResolution.height + " (" + Screen.currentResolution.refreshRate + "Hz)";
        int currentResolutionIndex = 0;

        //Invert the list and make it fit to the UI values
        for (int j = 0; j < resOptions.Count; j++)
        {
            if (resOptions[j] == currentResOption)
            {
                currentResolutionIndex = j;
                Debug.Log("Ladies and gentlemen, we got em! Current Reslution: " + currentResOption);
            }
        }

        resolutionDropdown.AddOptions(resOptions);
        resolutionDropdown.value = currentResolutionIndex;

        DataManager.LoadSettingsData();
        ConfigGraphicsSettings();

        //Set quality setting value in UI so it matches current settings
        switch (qualityGrade)
        {
            case 0:
                qualityDropdown.value = 0;
                break;

            case 1:
                qualityDropdown.value = 1;
                break;

            case 2:
                qualityDropdown.value = 2;
                break;

            case 3:
                qualityDropdown.value = 3;
                break;
        }

        //Set fullscreen setting value in UI so it matches current settings
        if (fullscreenSettings == FullScreenMode.ExclusiveFullScreen)
        {
            screenModeDropdown.value = 0;
        }

        else if (fullscreenSettings == FullScreenMode.FullScreenWindow)
        {
            screenModeDropdown.value = 1;
        }

        else if (fullscreenSettings == FullScreenMode.Windowed)
        {
            screenModeDropdown.value = 2;
        }

        ApplyChanges();

        masterBar.onValueChanged.AddListener(delegate { MasterBarValueChangeCheck(); });
        musicBar.onValueChanged.AddListener(delegate { MusicBarValueChangeCheck(); });
        soundFXBar.onValueChanged.AddListener(delegate { SoundFXBarValueChangeCheck(); });
        ambientBar.onValueChanged.AddListener(delegate { AmbientBarValueChangeCheck(); });
    }

    void Start()
    {
        DataManager.LoadSettingsData();
        ConfigAudioSettings();
    }

    //VALUE CHANGED FOR SOUND
    public void MasterBarValueChangeCheck()
    {
        if (masterBar.value < musicBar.value)
        {
            musicBar.value = masterBar.value;
            musicLevel = musicBar.value;
            mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);
        }

        if (masterBar.value < soundFXBar.value)
        {
            soundFXBar.value = masterBar.value;
            soundFXLevel = soundFXBar.value;
            mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);
        }

        if (masterBar.value < ambientBar.value)
        {
            ambientBar.value = masterBar.value;
            ambientLevel = ambientBar.value;
            mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);
        }

        masterLevel = masterBar.value;

        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterLevel) * 20);

        Debug.Log("Master Volume: " + masterLevel);
    }

    public void MusicBarValueChangeCheck()
    {
        if (musicBar.value >= masterBar.value)
        {
            musicBar.value = masterBar.value;
        }

        musicLevel = musicBar.value;
        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);

        Debug.Log("Music Volume: " + musicLevel);
    }

    public void SoundFXBarValueChangeCheck()
    {
        if (soundFXBar.value >= masterBar.value)
        {
            soundFXBar.value = masterBar.value;
        }

        soundFXLevel = soundFXBar.value;
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);

        Debug.Log("SFX Volume: " + soundFXLevel);
    }

    public void AmbientBarValueChangeCheck()
    {
        if (ambientBar.value >= masterBar.value)
        {
            ambientBar.value = masterBar.value;
        }

        ambientLevel = ambientBar.value;
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);

        Debug.Log("Ambient Volume: " + ambientLevel);
    }

    //VALUE CHANGE FOR GRAPHICS
    public void ResolutionValueChangeCheck()
    {
        resolutionChoice = resolutionDropdown.value;

        string resSettings;
        Resolution res;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            res = Screen.resolutions[i];
            resSettings = res.width + " x " + res.height + " (" + res.refreshRate + "Hz)";

            if (resSettings == resolutionDropdown.options[resolutionChoice].text)
            {
                resolutionCurrent.width = res.width;
                resolutionCurrent.height = res.height;
                hertz = res.refreshRate;
            }
        }

        Debug.Log("CURRENT RESOLUTION : " + resolutionCurrent.width + " x " + resolutionCurrent.height + " (" + hertz + "Hz)");
    }

    public void QualityValueChangeCheck()
    {
        qualityChoice = qualityDropdown.value;

        switch (qualityChoice)
        {
            //Very High
            case 0:
                qualityGrade = 0;
                break;

            //High
            case 1:
                qualityGrade = 1;
                break;

            //Medium
            case 2:
                qualityGrade = 2;
                break;

            //Low
            case 3:
                qualityGrade = 3;
                break;
        }

        Debug.Log("CURRENT QUALITY : " + qualityDropdown.options[qualityChoice].text);
    }

    public void ScreenModeValueChangeCheck()
    {
        screenModeChoice = screenModeDropdown.value;

        switch (screenModeChoice)
        {
            case 0:
                fullscreenSettings = FullScreenMode.ExclusiveFullScreen;
                break;

            case 1:
                fullscreenSettings = FullScreenMode.FullScreenWindow;
                break;

            case 2:
                fullscreenSettings = FullScreenMode.Windowed;
                break;
        }

        Debug.Log("CURRENT SCREENMODE : " + screenModeDropdown.options[screenModeChoice].text);
    }

    //GENERAL FUNCTIONS
    public void ConfigGraphicsSettings()
    {
        //Resolution Config
        res.width = resolutionCurrent.width;
        res.height = resolutionCurrent.height;
        res.refreshRate = hertz;

        resSettings = res.width + " x " + res.height + " (" + res.refreshRate + "Hz)";

        for (int i = 0; i < resolutionDropdown.options.Count; i++)
        {
            if (resolutionDropdown.options[i].text == resSettings)
            {
                resolutionDropdown.value = i;
            }
        }

        //Quality Config
        for (int i = 0; i < qualityDropdown.options.Count; i++)
        {
            if (qualityGrade == i)
            {
                qualityChoice = i;
            }
        }

        //Fullscreen Config
        for (int i = 0; i < screenModeDropdown.options.Count; i++)
        {
            if (screenModeChoice == i)
            {
                switch (screenModeChoice)
                {
                    case 0:
                        fullscreenSettings = FullScreenMode.ExclusiveFullScreen;
                        break;

                    case 1:
                        fullscreenSettings = FullScreenMode.FullScreenWindow;
                        break;

                    case 2:
                        fullscreenSettings = FullScreenMode.Windowed;
                        break;
                }
            }
        }
    }

    public void ConfigAudioSettings()
    {
        //Sound Config
        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterLevel) * 20);
        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);

        masterBar.value = masterLevel;
        musicBar.value = musicLevel;
        soundFXBar.value = soundFXLevel;
        ambientBar.value = ambientLevel;

        Debug.Log("LOADED VALUE MASTER: " + masterLevel);
        Debug.Log("LOADED VALUE MUSIC: " + musicLevel);
        Debug.Log("LOADED VALUE FX: " + soundFXLevel);
        Debug.Log("LOADED VALUE AMB: " + ambientLevel);
    }

    public void ApplyChanges()
    {
        Screen.SetResolution(resolutionCurrent.width, resolutionCurrent.height, fullscreenSettings, hertz);
        QualitySettings.SetQualityLevel(qualityGrade);
        Debug.Log("CHANGED TO : " + resolutionCurrent.width + " x " + resolutionCurrent.height + " (" + hertz + "Hz) with Quality Level - " + qualityGrade.ToString() + " and screen mode " + fullscreenSettings);

        masterBar.value = masterLevel;
        musicBar.value = musicLevel;
        soundFXBar.value = soundFXLevel;
        ambientBar.value = ambientLevel;

        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicBar.value) * 20);
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXBar.value) * 20);
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientBar.value) * 20);
        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterBar.value) * 20);

        DataManager.SaveSettingsData();
    }

    //ALL MENUES WE CAN COME TO
    public void Pause()
    {
        paused = true;
        Time.timeScale = 0.0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        pC.enabled = false;
        keyAndMouseRot.enabled = false;
        controllerRot.enabled = false;
        pauseCanvas.SetActive(true);
        loadingScreenCanvas.SetActive(false);
        confirmQuitConvas.SetActive(false);
        playerUICanvas.SetActive(false);

        if (bossHPCanvas.activeSelf)
        {
            arenaFight = true;
            bossHPCanvas.SetActive(false);
        }
    }

    public void Resume()
    {
        paused = false;
        Time.timeScale = 1.0f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        pC.enabled = true;
        keyAndMouseRot.enabled = true;
        controllerRot.enabled = true;
        pauseCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(false);
        confirmQuitConvas.SetActive(false);
        playerUICanvas.SetActive(true);

        if (!bossHPCanvas.activeSelf && arenaFight == true)
        {
            arenaFight = false;
            bossHPCanvas.SetActive(true);
        }
    }

    public void LoadingScreen()
    {
        pauseCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(true);
        confirmQuitConvas.SetActive(false);

        Time.timeScale = 1.0f;

        DataManager.SaveSettingsData();

        StartCoroutine(LoadAsync("MainMenuFox"));
    }

    public void MainMenu()
    {
        pauseCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(false);
        confirmQuitConvas.SetActive(true);
    }

    //LOADING FUNCTION
    IEnumerator LoadAsync(string levelToLoad)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelToLoad);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            loadingBar.value = progress;

            yield return null;
        }
    }
}