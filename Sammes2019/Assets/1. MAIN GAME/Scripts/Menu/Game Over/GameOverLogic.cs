﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameOverLogic : MonoBehaviour
{
    public GameObject player;

    [Header("Components - AUTO")]
    public GameObject gameOverCanvas;
    public GameObject loadingScreenCanvas;
    private GameObject playerUICanvas;

    [Header("Game Over Screen")]
    public TMPro.TextMeshProUGUI gameStats;

    [Header("Loading Screen")]
    private string levelToLoad;
    public Slider loadingBar;

    [HideInInspector]
    public PlayerStats playerStats;
    public Cinemachine.CinemachineFreeLook keyAndMouseRot;
    public Cinemachine.CinemachineFreeLook controllerRot;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.Find("Fox");
        playerStats = player.GetComponent<PlayerStats>();
        loadingScreenCanvas.SetActive(false);
        gameOverCanvas.SetActive(false);
        playerUICanvas = GameObject.Find("PlayerUI");
    }

    public void GameOver(int h, int m, float sec)
    {
        playerStats.timerH = h;
        playerStats.timerM = m;
        playerStats.timerS = sec;

        DataManager.SaveGameData();

        loadingScreenCanvas.SetActive(false);
        gameOverCanvas.SetActive(true);
        playerUICanvas.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        keyAndMouseRot.enabled = false;
        controllerRot.enabled = false;

        gameStats.text = "\n Round " + playerStats.roundCompleted+ " \n Time: " +playerStats.time+ "\n Enemies Killed: " +playerStats.killCount;

        DataManager.DeleteFile(DataManager.gameFileName);
    }

    //ALL MENUES WE CAN COME TO
    public void Restart()
    {
        gameOverCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(true);
        StartCoroutine(LoadAsync(SceneManager.GetActiveScene().name));
    }

    public void MainMenu()
    {
        gameOverCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(true);
        StartCoroutine(LoadAsync("MainMenuFox"));
    }

    //LOADING FUNCTION
    IEnumerator LoadAsync(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            loadingBar.value = progress;

            yield return null;
        }
    }
}