﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;

public class MainMenuLogic : MonoBehaviour
{
    public GameObject[] playButtonsText;
    public GameObject[] deleteButtons;

    [Header("Components Arrays - AUTO")]
    public Button[] interactables;
    public AudioMixer mainMixer;
    public string sceneNameLoad;

    [Header("Transitions")]
    public Camera mainCamera;
    public bool menuTrans;
    public float rotSpeed;
    public float rotTime;
    public Transform camCurrent;
    public Transform target;
    public Vector3 relativePos;
    public Quaternion targetRot;

    [Header("Main Menu Settings")]
    public bool mainMenuNav;
    public Canvas mainMenuCanvas;
    public Transform camMainRot;
    public AudioSource menuMusic;
    public float startVolumeMenu;

    [Header("Play Menu Settings")]
    public bool playMenuNav;
    public Canvas playMenuCanvas;
    public Transform camPlayRot;
    public string gameFileRef;
    public GameObject[] gameButtons;

    [Header("Play - Confirm Game")]
    public bool confirmGameNav;
    public Canvas confirmGameCanvas;
    public Transform camConfirmRot;

    [Header("Play - Data Managment")]
    public bool dataManageNav;
    public Canvas dataManageCanvas;
    public Transform camDataRot;

    [Header("Play - Loading Screen")]
    public bool loadingScreenNav;
    public Canvas loadingScreenCanvas;
    public Transform camLSRot;
    public Slider loadingBar;
    private string levelToLoad;

    [Header("Settings Settings")]
    public bool settingsNav;
    public Canvas settingsCanvas;
    public Transform camSettingsRot;
    public GameObject renderPreviewModel;

    [Header("Settings - Sound & Music")]
    public Slider masterBar;
    public Slider musicBar;
    public Slider soundFXBar;
    public Slider ambientBar;

    public float masterLevel;
    public float musicLevel;
    public float soundFXLevel;
    public float ambientLevel;

    [Header("Settings - Graphics")]
    public TMPro.TMP_Dropdown resolutionDropdown;
    public TMPro.TMP_Dropdown qualityDropdown;
    public TMPro.TMP_Dropdown screenModeDropdown;

    public int resolutionChoice;
    public int qualityChoice;
    public int screenModeChoice;

    [Header("Graphics - Current Values")]
    public string resSettings;
    public Resolution[] resolutionsAvalible;
    public List<string> resOptions = new List<string>();
    public Resolution res;
    public Resolution resolutionCurrent;
    public int qualityGrade;
    public FullScreenMode fullscreenSettings;
    public int hertz;

    [Header("Credits Settings")]
    public bool creditsNav;
    public Canvas creditsCanvas;
    public Transform camCreditsRot;
    public GameObject creditsText;
    public AudioSource creditsMusic;
    public float startVolumeCred;

    [Header("Quit Settings")]
    public bool quitNav;
    public Transform camQuitRot;

    [Header("Close Game Settings")]
    public bool closeGameNav;
    public Transform camCloseGameRot;

    // Start is called before the first frame update
    void Awake()
    {
        //Find the UI Components
        interactables = FindObjectsOfType<Button>();

        //cs = GameObject.Find("---CREDIT SLIDES---").GetComponent<CreditsFadeSlide>();

        resolutionDropdown.onValueChanged.AddListener(delegate { ResolutionValueChangeCheck(); });
        qualityDropdown.onValueChanged.AddListener(delegate { QualityValueChangeCheck(); });
        screenModeDropdown.onValueChanged.AddListener(delegate { ScreenModeValueChangeCheck(); });

        //Set resolutions setting value in UI so it matches current settings
        resolutionCurrent.width = Screen.currentResolution.width;
        resolutionCurrent.height = Screen.currentResolution.height;
        qualityGrade = QualitySettings.GetQualityLevel();
        hertz = Screen.currentResolution.refreshRate;

        resolutionsAvalible = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        float resXcal;
        float resYcal;
        double aspectRa;
        int hertzValidation;

        //Get a list of all resolutions avalible
        for (int i = 0; i < resolutionsAvalible.Length; i++)
        {
            resXcal = resolutionsAvalible[i].width;
            resYcal = resolutionsAvalible[i].height;
            aspectRa = resXcal / resYcal;
            hertzValidation = resolutionsAvalible[i].refreshRate;

            //Only accept 16:9 resolutions
            if (aspectRa < 1.771 || aspectRa > 1.778)
            {
                continue;
            }

            //Only accept Hz that's 59 or higher
            if (hertzValidation < 59)
            {
                continue;
            }

            string resOption = resolutionsAvalible[i].width + " x " + resolutionsAvalible[i].height + " (" + resolutionsAvalible[i].refreshRate + "Hz)";
            resOptions.Add(resOption);
        }

        //Set list to go from highest at the top to lowest at the bottom
        resOptions.Reverse();
        string currentResOption = Screen.currentResolution.width + " x " + Screen.currentResolution.height + " (" + Screen.currentResolution.refreshRate + "Hz)";
        int currentResolutionIndex = 0;

        //Invert the list and make it fit to the UI values
        for (int j = 0; j < resOptions.Count; j++)
        {
            if(resOptions[j] == currentResOption)
            {
                currentResolutionIndex = j;
                Debug.Log("Ladies and gentlemen, we got em! Current Reslution: " + currentResOption);
            }
        }

        resolutionDropdown.AddOptions(resOptions);
        resolutionDropdown.value = currentResolutionIndex;

        DataManager.LoadSettingsData();
        ConfigGraphicsSettings();

        //Set quality setting value in UI so it matches current settings
        switch (qualityGrade)
        {
            case 0:
                qualityDropdown.value = 0;
                break;

            case 1:
                qualityDropdown.value = 1;
                break;

            case 2:
                qualityDropdown.value = 2;
                break;

            case 3:
                qualityDropdown.value = 3;
                break;
        }

        //Set fullscreen setting value in UI so it matches current settings
        if (fullscreenSettings == FullScreenMode.ExclusiveFullScreen)
        {
            screenModeDropdown.value = 0;
        }

        else if (fullscreenSettings == FullScreenMode.FullScreenWindow)
        {
            screenModeDropdown.value = 1;
        }

        else if(fullscreenSettings == FullScreenMode.Windowed)
        {
            screenModeDropdown.value = 2;
        }

        ApplyChanges();

        masterBar.onValueChanged.AddListener(delegate { MasterBarValueChangeCheck(); });
        musicBar.onValueChanged.AddListener(delegate { MusicBarValueChangeCheck(); });
        soundFXBar.onValueChanged.AddListener(delegate { SoundFXBarValueChangeCheck(); });
        ambientBar.onValueChanged.AddListener(delegate { AmbientBarValueChangeCheck(); });

        menuTrans = false;
    }

    void Start()
    {
        //Ready, set, ACTION!
        DataManager.LoadSettingsData();
        ConfigAudioSettings();
        MainMenu();
        menuMusic.Play();
    }

    void Update()
    {
        //Update for Menu Navigation and handle transitions 
        if (menuTrans)
        {
            camCurrent = mainCamera.transform;
            rotTime += Time.deltaTime * rotSpeed;

            //MAIN MENU
            if (mainMenuNav)
            {
                target = camMainRot;
                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);
                creditsMusic.volume -= startVolumeCred * Time.deltaTime * 1.5f;
                menuMusic.volume += startVolumeMenu * Time.deltaTime * 1.5f;

                if (CamHasTransed())
                {
                    menuTrans = false;
                    dataManageCanvas.enabled = false;
                    confirmGameCanvas.enabled = false;
                    creditsCanvas.enabled = true;
                    settingsCanvas.enabled = true;
                    renderPreviewModel.SetActive(true);
                    creditsText.SetActive(false);
                    creditsText.transform.localPosition = new Vector3(0, -1241);
                    creditsText.GetComponent<CreditsScroll>().endOfCredits = false;
                    creditsMusic.Stop();
                    ToggleButtons();
                }
            }

            //PLAY / LOADIN SCREEN MENU
            else if(playMenuNav || loadingScreenNav)
            {
                if (playMenuNav && loadingScreenNav == false)
                {
                    loadingScreenCanvas.enabled = false;
                    playMenuCanvas.enabled = true;

                    target = camPlayRot;
                }

                else if (loadingScreenNav && playMenuNav == false)
                {
                    playMenuCanvas.enabled = false;
                    loadingScreenCanvas.enabled = true;

                    target = camLSRot;
                }

                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);

                if (CamHasTransed())
                {
                    menuTrans = false;
                    dataManageCanvas.enabled = true;
                    confirmGameCanvas.enabled = true;
                    mainMenuCanvas.enabled = false;
                    creditsCanvas.enabled = false;
                    settingsCanvas.enabled = false;
                    renderPreviewModel.SetActive(false);
                    ToggleButtons();

                    if(loadingScreenNav)
                    {
                        StartCoroutine(LoadAsync(levelToLoad));
                    }
                }
            }

            //SETTINGS / DATA MANAGE MENU
            else if (settingsNav || dataManageNav)
            {
                if(settingsNav && dataManageNav == false)
                {
                    dataManageCanvas.enabled = false;
                    settingsCanvas.enabled = true;
                    target = camSettingsRot;
                }

                else if(dataManageNav && settingsNav == false)
                {
                    settingsCanvas.enabled = false;
                    dataManageCanvas.enabled = true;

                    target = camDataRot;
                }

                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);

                if (CamHasTransed())
                {
                    menuTrans = false;
                    ToggleButtons();
                }
            }

            //CREDITS / CONFIRM MENU
            else if (creditsNav || confirmGameNav )
            {
                if (creditsNav && confirmGameNav == false)
                {
                    confirmGameCanvas.enabled = false;
                    creditsCanvas.enabled = true;

                    creditsMusic.volume += startVolumeCred * Time.deltaTime * 1.5f;
                    menuMusic.volume -= startVolumeMenu * Time.deltaTime * 1.5f;

                    target = camCreditsRot;
                }

                else if (confirmGameNav && creditsNav == false)
                {
                    creditsCanvas.enabled = false;
                    confirmGameCanvas.enabled = true;

                    target = camConfirmRot;
                }

                target = camCreditsRot;
                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);

                if (CamHasTransed())
                {
                    menuTrans = false;
                    ToggleButtons();

                    if (creditsNav)
                        creditsText.SetActive(true);
                }
            }

            //QUIT MENU
            else if(quitNav)
            {
                target = camQuitRot;
                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);

                if (CamHasTransed())
                {
                    menuTrans = false;
                    ToggleButtons();
                }
            }

            //CLOSE GAME CAM POSE
            else if(closeGameNav)
            {
                target = camCloseGameRot;
                relativePos = target.position - mainCamera.transform.position;
                targetRot = Quaternion.LookRotation(relativePos);

                // Move our position a step closer to the target.
                mainCamera.transform.rotation = Quaternion.Slerp(camCurrent.rotation, targetRot, rotTime);

                if (CamHasTransed())
                {
                    menuTrans = false;
                    Application.Quit();
                }
            }

            //ERROR, U FUCKED UP
            else
            {
                Debug.LogError("SOMETHING WRONG! FIX IT STUPID");
            }
        }
    }

    //VALUE CHANGED FOR SOUND
    public void MasterBarValueChangeCheck()
    {
        if(masterBar.value < musicBar.value)
        {
            musicBar.value = masterBar.value;
            musicLevel = musicBar.value;
            mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);
        }

        if(masterBar.value < soundFXBar.value)
        {
            soundFXBar.value = masterBar.value;
            soundFXLevel = soundFXBar.value;
            mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);
        }

        if (masterBar.value < ambientBar.value)
        {
            ambientBar.value = masterBar.value;
            ambientLevel = ambientBar.value;
            mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);
        }

        masterLevel = masterBar.value;

        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterLevel) * 20);

        Debug.Log("Master Volume: " +masterLevel);
    }

    public void MusicBarValueChangeCheck()
    {
        if (musicBar.value >= masterBar.value)
        {
            musicBar.value = masterBar.value;
        }

        musicLevel = musicBar.value;
        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);

        Debug.Log("Music Volume: " +musicLevel);
    }

    public void SoundFXBarValueChangeCheck()
    {
        if (soundFXBar.value >= masterBar.value)
        {
            soundFXBar.value = masterBar.value;
        }

        soundFXLevel = soundFXBar.value;
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);

        Debug.Log("SFX Volume: " +soundFXLevel);
    }

    public void AmbientBarValueChangeCheck()
    {
        if (ambientBar.value >= masterBar.value)
        {
            ambientBar.value = masterBar.value;
        }

        ambientLevel = ambientBar.value;
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);

        Debug.Log("Ambient Volume: " + ambientLevel);
    }

    //VALUE CHANGE FOR GRAPHICS
    public void ResolutionValueChangeCheck()
    {
        resolutionChoice = resolutionDropdown.value;

        for(int i = 0; i < Screen.resolutions.Length; i++)
        {
            res = Screen.resolutions[i];
            resSettings = res.width + " x " + res.height + " (" + res.refreshRate + "Hz)";

            if (resSettings == resolutionDropdown.options[resolutionChoice].text)
            {
                resolutionCurrent.width = res.width;
                resolutionCurrent.height = res.height;
                hertz = res.refreshRate;
            }
        }

        Debug.Log("CURRENT RESOLUTION : " + resSettings);
    }

    public void QualityValueChangeCheck()
    {
        qualityChoice = qualityDropdown.value;

        switch (qualityChoice)
        {
            //Very High
            case 0:
                qualityGrade = 0;
                break;

            //High
            case 1:
                qualityGrade = 1;
                break;

            //Medium
            case 2:
                qualityGrade = 2;
                break;

            //Low
            case 3:
                qualityGrade = 3;
                break;
        }

        Debug.Log("CURRENT QUALITY : " + qualityDropdown.options[qualityChoice].text);
    }

    public void ScreenModeValueChangeCheck()
    {
        screenModeChoice = screenModeDropdown.value;

        switch (screenModeChoice)
        {
            case 0:
                fullscreenSettings = FullScreenMode.ExclusiveFullScreen;
                break;

            case 1:
                fullscreenSettings = FullScreenMode.FullScreenWindow;
                break;

            case 2:
                fullscreenSettings = FullScreenMode.Windowed;
                break;
        }

        Debug.Log("CURRENT SCREENMODE : " + screenModeDropdown.options[screenModeChoice].text);
    }

    //GENERAL FUNCTIONS
    public void ConfigGraphicsSettings()
    {
        //Resolution Config
        res.width = resolutionCurrent.width;
        res.height = resolutionCurrent.height;
        res.refreshRate = hertz;

        resSettings = res.width + " x " + res.height + " (" + res.refreshRate + "Hz)";

        for (int i = 0; i < resolutionDropdown.options.Count; i++)
        {
            if (resolutionDropdown.options[i].text == resSettings)
            {
                resolutionDropdown.value = i;
            }
        }

        //Quality Config
        for (int i = 0; i < qualityDropdown.options.Count; i++)
        {
            if (qualityGrade == i)
            {
                qualityChoice = i;
            }
        }

        //Fullscreen Config
        for (int i = 0; i < screenModeDropdown.options.Count; i++)
        {
            if (screenModeChoice == i)
            {
                switch (screenModeChoice)
                {
                    case 0:
                        fullscreenSettings = FullScreenMode.ExclusiveFullScreen;
                        break;

                    case 1:
                        fullscreenSettings = FullScreenMode.FullScreenWindow;
                        break;

                    case 2:
                        fullscreenSettings = FullScreenMode.Windowed;
                        break;
                }
            }
        }
    }

    //GENERAL FUNCTIONS
    public void ConfigAudioSettings()
    {
        //Sound Config
        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterLevel) * 20);
        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicLevel) * 20);
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXLevel) * 20);
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientLevel) * 20);

        masterBar.value = masterLevel;
        musicBar.value = musicLevel;
        soundFXBar.value = soundFXLevel;
        ambientBar.value = ambientLevel;

        Debug.Log("LOADED VALUE MASTER: " + masterLevel);
        Debug.Log("LOADED VALUE MUSIC: " + musicLevel);
        Debug.Log("LOADED VALUE FX: " + soundFXLevel);
        Debug.Log("LOADED VALUE AMB: " + ambientLevel);
    }

    public void ApplyChanges()
    {
        Screen.SetResolution(resolutionCurrent.width, resolutionCurrent.height, fullscreenSettings, hertz);
        QualitySettings.SetQualityLevel(qualityGrade);
        Debug.Log("CHANGED TO : " + resolutionCurrent.width + " x " + resolutionCurrent.height + " (" + hertz + "Hz) with Quality Level - " +qualityGrade.ToString()+ " and screen mode " +fullscreenSettings);

        masterBar.value = masterLevel;
        musicBar.value = musicLevel;
        soundFXBar.value = soundFXLevel;
        ambientBar.value = ambientLevel;

        mainMixer.SetFloat("MusicVolume", Mathf.Log10(musicBar.value) * 20);
        mainMixer.SetFloat("SFXVolume", Mathf.Log10(soundFXBar.value) * 20);
        mainMixer.SetFloat("AmbientVolume", Mathf.Log10(ambientBar.value) * 20);
        mainMixer.SetFloat("MasterVolume", Mathf.Log10(masterBar.value) * 20);

        DataManager.SaveSettingsData();
    }

    public void LoadGameFile()
    {
        DataManager.AssignFileRef(gameFileRef);
        Debug.Log("Save File has been assigned!");
    }

    public bool CamHasTransed()
    {
        if (camCurrent.rotation == targetRot)
        {
            rotTime = 0;
            camCurrent.rotation = targetRot;
            return true;
        }

        else
        {
            return false;
        }
    }

    public void ToggleButtons()
    {
        if(menuTrans)
        {
            foreach (Button x in interactables)
            {
                x.enabled = false;
            }
        }

        else
        {
            foreach (Button x in interactables)
            {
                x.enabled = true;
            }
        }
    }

    //ALL MENUES WE CAN COME TO
    public void MainMenu()
    {
        mainMenuNav = true;
        playMenuNav = false;
        settingsNav = false;
        creditsNav = false;
        quitNav = false;

        menuTrans = true;

        mainMenuCanvas.enabled = true;
        ToggleButtons();
    }

    public void PlayMenu()
    {
        mainMenuNav = false;
        dataManageNav = false;
        confirmGameNav = false;
        playMenuNav = true;
        menuTrans = true;

        DataManager.LoadGameOnStart();

        for(int i = 0; i < 3; i++)
        {
            if(playButtonsText[i].GetComponent<TMPro.TextMeshProUGUI>().text == "EMPTY GAME FILE")
            {
                deleteButtons[i].SetActive(false);
            }

            else
            {
                deleteButtons[i].SetActive(true);
            }
        }

        ToggleButtons();
    }

    public void SettingsMenu()
    {
        mainMenuNav = false;
        settingsNav = true;
        menuTrans = true;

        ToggleButtons();
    }

    public void CreditsMenu()
    {
        mainMenuNav = false;
        creditsNav = true;
        menuTrans = true;

        ToggleButtons();

        creditsText.SetActive(true);
        creditsText.transform.localPosition = new Vector3(0, -1241);
        creditsMusic.volume = startVolumeCred;
        creditsMusic.Play();
    }

    public void QuitMenu()
    {
        mainMenuNav = false;
        quitNav = true;
        menuTrans = true;

        settingsCanvas.enabled = false;
        creditsCanvas.enabled = false;

        ToggleButtons();
    }

    public void GameConfirmMenu()
    {
        playMenuNav = false;
        confirmGameNav = true;
        menuTrans = true;

        ToggleButtons();
    }

    public void DataManagmentMenu()
    {
        playMenuNav = false;
        dataManageNav = true;
        menuTrans = true;

        ToggleButtons();
    }

    public void LoadingScreenMenu()
    {
        levelToLoad = sceneNameLoad;
        confirmGameNav = false;
        loadingScreenNav = true;
        menuTrans = true;

        ToggleButtons();
    }

    //LOADING FUNCTION
    IEnumerator LoadAsync(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            loadingBar.value = progress;

            yield return null;
        }
    }

    //CONFIRM IF YOU WANNA START GAME OR NOT
    public void GameConf(int gameFileIndex)
    {
        if (gameFileIndex == 0)
        {
            gameFileRef = "File 1";
        }

        else if (gameFileIndex == 1)
        {
            gameFileRef = "File 2";
        }

        else if (gameFileIndex == 2)
        {
            gameFileRef = "File 3";
        }

        LoadGameFile();
        GameConfirmMenu();

        GameObject.Find("CONFIRM_Text").GetComponent<TMPro.TextMeshProUGUI>().text = "Play on " + gameFileRef + "?";
    }

    //HANDLE GAME DATA
    public void ConfirmDelete(int gameFileIndex)
    {
        gameFileRef = "";

        if (gameFileIndex == 0)
        {
            gameFileRef = "File 1";
            //LOAD FILE
        }

        else if (gameFileIndex == 1)
        {
            gameFileRef = "File 2";
            //LOAD FILE
        }

        else if (gameFileIndex == 2)
        {
            gameFileRef = "File 3";
            //LOAD FILE
        }

        LoadGameFile();
        DataManagmentMenu();

        GameObject.Find("DATA_Text").GetComponent<TMPro.TextMeshProUGUI>().text = "Delete " + gameFileRef + "? This can't be undone, like ever...";
    }

    //DELETE THE FILE YOU SELECTED
    public void DeleteFile()
    {
        //DELETE FILE
        DataManager.DeleteFile(gameFileRef);

        PlayMenu();
    }

    //QUIT GAME
    public void Order66()
    {
        quitNav = false;
        closeGameNav = true;
        menuTrans = true;

        renderPreviewModel.SetActive(false);

        ToggleButtons();
    }
}