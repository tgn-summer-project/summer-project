﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsData
{
    //SOUND SETTINGS
    public float masterSliderLevel;
    public float musicSliderLevel;
    public float sfxSliderLevel;
    public float ambientSliderLevel;

    //GRAPHICS SETTINGS
    public int resWidth;
    public int resHeight;
    public int hertzSettings;
    public int qualitySettings;
    public int fullscreenSettings;

    public SettingsData(MainMenuLogic mml)
    {
        masterSliderLevel = mml.masterLevel;
        musicSliderLevel = mml.musicLevel;
        sfxSliderLevel = mml.soundFXLevel;
        ambientSliderLevel = mml.ambientLevel;

        resWidth = mml.resolutionCurrent.width;
        resHeight = mml.resolutionCurrent.height;
        hertzSettings = mml.hertz;
        qualitySettings = mml.qualityGrade;
        fullscreenSettings = mml.screenModeChoice;
    }

    public SettingsData(PauseMenuLogic pml)
    {
        masterSliderLevel = pml.masterLevel;
        musicSliderLevel = pml.musicLevel;
        sfxSliderLevel = pml.soundFXLevel;
        ambientSliderLevel = pml.ambientLevel;

        resWidth = pml.resolutionCurrent.width;
        resHeight = pml.resolutionCurrent.height;
        hertzSettings = pml.hertz;
        qualitySettings = pml.qualityGrade;
        fullscreenSettings = pml.screenModeChoice;
    }
}