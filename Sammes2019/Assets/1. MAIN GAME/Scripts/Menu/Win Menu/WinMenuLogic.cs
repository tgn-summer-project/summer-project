﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class WinMenuLogic : MonoBehaviour
{
    public GameObject player;
    private PlayerInput pI;
    public Cinemachine.CinemachineFreeLook keyAndMouseRot;
    public Cinemachine.CinemachineFreeLook controllerRot;

    [Header("Components - AUTO")]
    public GameObject winMenuCanvas;
    public GameObject loadingScreenCanvas;
    private GameObject playerUICanvas;

    [Header("Win Screen")]
    public TMPro.TextMeshProUGUI gameStats;

    [Header("Loading Screen")]
    private string levelToLoad;
    public Slider loadingBar;

    [HideInInspector]
    public PlayerStats playerStats;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.Find("Fox");
        pI = player.GetComponent<PlayerInput>();
        playerStats = player.GetComponent<PlayerStats>();
        loadingScreenCanvas.SetActive(false);
        winMenuCanvas.SetActive(false);
        playerUICanvas = GameObject.Find("PlayerUI");

    }

    public void WinnerWinnerChickenDinner(int h, int m, float sec)
    {
        playerStats.timerH = h;
        playerStats.timerM = m;
        playerStats.timerS = sec;

        DataManager.SaveGameData();

        loadingScreenCanvas.SetActive(false);
        winMenuCanvas.SetActive(true);
        playerUICanvas.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        pI.enabled = false;
        keyAndMouseRot.enabled = false;
        controllerRot.enabled = false;

        gameStats.text = "\n Round " + playerStats.roundCompleted + " \n Time: " + playerStats.time + "\n Enemies Killed: " + playerStats.killCount;
    }

    //ALL MENUES WE CAN COME TO
    public void MainMenu()
    {
        winMenuCanvas.SetActive(false);
        loadingScreenCanvas.SetActive(true);
        StartCoroutine(LoadAsync("MainMenuFox"));
    }

    //LOADING FUNCTION
    IEnumerator LoadAsync(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            loadingBar.value = progress;

            yield return null;
        }
    }
}