﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraOffset : CinemachineExtension
{
    public Vector3 Offset = new Vector3(0f, -0.25f, -1.25f);

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        state.PositionCorrection += state.FinalOrientation * Offset;
    }
}
