﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class CharacterStat
{
    public float BaseValue;
    public UnityEvent OnAddedMod, OnRemovedMod;

    public virtual float Value
    {
        get
        {
            if (isDirty || BaseValue != lastBaseValue)
            {
                lastBaseValue = BaseValue;
                value = Calculate();
                isDirty = false;

                return value;
            }

            return value;
        }
    }

    protected float value;
    protected float lastBaseValue = float.MinValue;
    protected bool isDirty = true;

    protected readonly List<StatModifier> statModifiers; // Can be changed
    protected readonly IReadOnlyCollection<StatModifier> StatModifiers; // Can't be changed

    public CharacterStat()
    {
        statModifiers = new List<StatModifier>();
        StatModifiers = statModifiers.AsReadOnly();
    }

    public CharacterStat(float baseValue) : this()
    {
        this.BaseValue = baseValue;
    }

    public virtual void AddMod(StatModifier mod)
    {
        isDirty = true;
        statModifiers.Add(mod);
        statModifiers.Sort(CompareModOrder);
        OnAddedMod.Invoke();
    }

    public virtual bool RemoveAllModsFromSource(object source)
    {
        var removed = false;

        // traverse remove
        for (var i = statModifiers.Count - 1; i >= 0; i--)
        {
            if (statModifiers[i].Source != source)
                continue;

            isDirty = true;
            removed = true;
            statModifiers.RemoveAt(i);
        }

        if (removed)
            OnRemovedMod.Invoke();

        return removed;
    }
    protected virtual int CompareModOrder(StatModifier a, StatModifier b)
    {
        if (a.Order < b.Order)
            return -1;
        return a.Order > b.Order ? 1 : 0;
    }

    public virtual bool RemoveMod(StatModifier mod)
    {
        if (!statModifiers.Remove(mod))
            return false;

        isDirty = true;
        OnRemovedMod.Invoke();
        return true;
    }

    protected virtual float Calculate()
    {
        var finalValue = BaseValue;
        var sumPercentAdd = 0f;

        for (var i = 0; i < statModifiers.Count; i++)
        {
            var mod = statModifiers[i];
            switch (mod.Type)
            {
                case ModType.Flat:
                    finalValue += mod.Value;
                    break;
                case ModType.PercentAdd:
                {
                    sumPercentAdd += mod.Value;
                    if (i + 1 >= statModifiers.Count || statModifiers[i + 1].Type != ModType.PercentAdd)
                    {
                        finalValue *= 1 + sumPercentAdd;
                        sumPercentAdd = 0;
                    }

                    break;
                }

                case ModType.PercentMult:
                    finalValue *= 1 + mod.Value;
                    break;
            }
        }

        return (float) Math.Round(finalValue, 4);
    }
}
