﻿[System.Serializable]
public class PlayerData
{
    public int timerH;
    public int timerM;
    public float timerS;

    public int round;
    public string time;
    public int killCount;

    public int skillPointsToSpend;
    public float xp;
    public float xpReq;
    public int lvl;

    public float hp;
    public float hpMax;
    public float runSpeed;
    public float critChance;
    public float attackSpeed;
    public float rangedDMG;
    public float meleeDMG;
    public float stamina;
    public float dexterity;
    public float intelligence;
    public float strength;

    public PlayerData(PlayerStats ps)
    {
        timerH = ps.timerH;
        timerM = ps.timerM;
        timerS = ps.timerS;

        round = ps.roundCompleted;
        time = ps.time;
        killCount = ps.killCount;

        skillPointsToSpend = ps.AvailableSkillPoints;
        xp = ps.Experience;
        xpReq = ps.ExpRequired;
        lvl = ps.Level;

        hp = ps.Health.Value;
        hpMax = ps.MaxHealth.Value;
        runSpeed = ps.RunningSpeed.Value;

        critChance = ps.Critchance.Value;
        attackSpeed = ps.AttackSpeed.Value;
        rangedDMG = ps.RangedDamage.Value;
        meleeDMG = ps.MeleeDamage.Value;

        stamina = ps.Dexterity.Value;
        dexterity = ps.Dexterity.Value;
        intelligence = ps.Intelligence.Value;
        strength = ps.Strength.Value;
    }
}