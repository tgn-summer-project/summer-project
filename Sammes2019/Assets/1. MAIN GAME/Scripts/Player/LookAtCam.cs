﻿using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    public Transform Transform;

    private Camera camera;
    private RaycastHit lastHitObj;
    private float lastAngle;

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // Player layer
        int layerMask = 1 << 9;

        // Reversed, we ignore the player for the raycast
        layerMask = ~layerMask;

        if (Physics.Raycast(camera.transform.position,
                            camera.transform.TransformDirection(Vector3.forward),
                            out var hit,
                            Mathf.Infinity,
                            layerMask))
        {
            if (Vector3.Distance(lastHitObj.point, hit.point) <= 5)
            {
                return;
            }

            lastHitObj = hit;

            var degrees = Vector3.Angle(Transform.position, hit.point);
            var max = Mathf.Max( 45, degrees);
            var min = Mathf.Min(-45, degrees);
            var fixedAngle = degrees < 0 ? min : max;
            lastAngle = fixedAngle;
        }

        if (lastAngle != 0)
            Look();
    }

    private void Look()
    {
        Transform.Rotate(0f, 0f, lastAngle * Time.deltaTime * 5, Space.Self);
    }
}
