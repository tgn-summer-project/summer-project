﻿using Gamekit3D;
using UnityEngine;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    public UnityEvent OnLevelUp;

    public bool IsBurstActive;

    public int timerH;
    public int timerM;
    public float timerS;

    public int AvailableSkillPoints;
    public float Experience;
    public float ExpRequired = 100f;
    public int Level = 1;
    public int roundCompleted = 0;
    public string time = "";
    public int killCount = 0;
    //public bool hasLeveledUp = false;

    public CharacterStat Health;
    public CharacterStat MaxHealth;
    public CharacterStat RunningSpeed;

    public CharacterStat Critchance;
    public CharacterStat AttackSpeed;
    public CharacterStat RangedDamage;
    public CharacterStat MeleeDamage;

    public CharacterStat Stamina; // Raises max hp & running speed
    public CharacterStat Dexterity; // Increase attack speed & critical chance
    public CharacterStat Intelligence; // Increases ranged attack
    public CharacterStat Strength; // Increases melee attack

    void Start()
    {
        GetComponent<Damageable>().currentHitPoints = Health.Value;
        GetComponent<Damageable>().maxHitPoints = (int) MaxHealth.Value;
    }

    //private Slider xpBar;
    //private TMPro.TextMeshProUGUI xpBarLevel;

    /*void Start()
    {
        if (gameObject.tag != "STATUE")
        {
            xpBar = GameObject.Find("XPBar").GetComponent<Slider>();
            xpBarLevel = GameObject.Find("XPBarLevel").GetComponent<TMPro.TextMeshProUGUI>();
            xpBarLevel.text = ("Level " + Level.ToString());

            Debug.Log("XP GAINED: " + Experience);
            Debug.Log("XP REQUIRED: " + ExpRequired);

            xpBar.maxValue = ExpRequired;
            xpBar.value += Experience;
            hasLeveledUp = false;
        }
    }

    public void AddExperience(float value)
    {
        Experience += value;
        xpBar.maxValue = ExpRequired;
        xpBar.value = Experience;

        Debug.Log("XP GAINED: " + Experience);
        Debug.Log("XP REQUIRED: " + ExpRequired);

        if (Experience >= ExpRequired)
        {
            Experience = Experience- ExpRequired;
            xpBar.value = Experience;
            ExpRequired = Level * 1.1f * ExpRequired;
            xpBar.maxValue = ExpRequired;
            Level++;
            xpBarLevel.text = ("Level " +Level.ToString());
            AvailableSkillPoints++;
            Health.BaseValue = MaxHealth.BaseValue;
            hasLeveledUp = true;
        }
    }*/
}
