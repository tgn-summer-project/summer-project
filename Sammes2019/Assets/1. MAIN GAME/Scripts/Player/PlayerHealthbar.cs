﻿using UnityEngine;

public class PlayerHealthbar : MonoBehaviour
{
    public SimpleHealthBar Healthbar;
    private PlayerStats playerStats;

    // Start is called before the first frame update
    void Start()
    {
        playerStats = GameObject.Find("Fox").GetComponent<PlayerStats>();

        void Refresh()
        {
            Healthbar.UpdateBar(playerStats.Health.Value, playerStats.MaxHealth.Value);
        }

        Refresh();
        playerStats.Health.OnAddedMod.AddListener(Refresh);
    }
}
