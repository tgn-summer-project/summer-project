﻿using UnityEngine;

namespace Gamekit3D
{
    [CreateAssetMenu]
    public class TranslatedPhrases : OriginalPhrases
    {
        public OriginalPhrases originalPhrases;
    }
}