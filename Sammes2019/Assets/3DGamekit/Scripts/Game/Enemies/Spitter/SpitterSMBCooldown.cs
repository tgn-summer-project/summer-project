﻿using UnityEngine;

namespace Gamekit3D
{
    public class SpitterSMBCooldown : SceneLinkedSMB<ShamanBehaviour>
    {
        public override void OnSLStateNoTransitionUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            m_MonoBehaviour.FindTarget();
            m_MonoBehaviour.CheckNeedFleeing();
        }
    }
}