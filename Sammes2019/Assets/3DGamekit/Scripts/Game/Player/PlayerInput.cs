﻿using UnityEngine;
using System.Collections;

using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    public static PlayerInput Instance => s_Instance;

    protected static PlayerInput s_Instance;

    public UnityEvent OnAimEnable;
    public UnityEvent OnAimDisable;

    private float nextTimeToFire;

    [HideInInspector]
    public bool playerControllerInputBlocked;

    protected Vector2 m_Movement;
    protected Vector2 m_Camera;
    protected bool m_Jump;
    protected bool m_Attack;
    protected bool m_Shoot;
    protected bool m_Aim;
    protected bool m_Sprint;
    protected bool m_RageMode;
    protected bool m_Push;
    protected bool m_Might;

    protected bool m_Pause;
    protected bool m_Skills;
    protected bool m_ExternalInputBlocked;

    private RageMode RageMode;
    private Push Push;
    private ElementalMight ElementalMight;
    private PlayerStats PlayerStats;

    public PauseMenuLogic pml;
    //public SkillpointController spc;

    public Vector2 MoveInput
    {
        get
        {
            if(playerControllerInputBlocked || m_ExternalInputBlocked)
                return Vector2.zero;
            return m_Movement;
        }
    }

    public Vector2 CameraInput
    {
        get
        {
            if(playerControllerInputBlocked || m_ExternalInputBlocked)
                return Vector2.zero;
            return m_Camera;
        }
    }

    public bool JumpInput => m_Jump && !playerControllerInputBlocked && !m_ExternalInputBlocked;
    public bool Attack => m_Attack && !playerControllerInputBlocked && !m_ExternalInputBlocked;
    public bool Shoot => m_Shoot && m_Aim && !playerControllerInputBlocked && !m_ExternalInputBlocked;
    public bool Aim => m_Aim && !playerControllerInputBlocked && !m_ExternalInputBlocked;
    public bool Sprint => m_Sprint && !playerControllerInputBlocked && !m_ExternalInputBlocked;
    public bool Pause => m_Pause || m_Skills;

    WaitForSeconds m_AttackInputWait;
    WaitForSeconds m_ShootInputWait;

    Coroutine m_AttackWaitCoroutine;


    const float k_AttackInputDuration = 0.03f;

    void Awake()
    {
        if (gameObject.tag != "STATUE")
        {
            m_AttackInputWait = new WaitForSeconds(k_AttackInputDuration);

            PlayerStats = GameObject.Find("Fox").GetComponent<PlayerStats>();
            RageMode = GetComponent<RageMode>();
            Push = GetComponent<Push>();
            ElementalMight = GetComponent<ElementalMight>();

            pml = GameObject.Find("GameMaster").GetComponent<PauseMenuLogic>();
            //spc = GameObject.Find("GameMaster").GetComponent<SkillpointController>();

            Debug.Log(pml.name);
            //Debug.Log(spc.name);

            if (s_Instance == null)
                s_Instance = this;
            else if (s_Instance != this)
                throw new UnityException("There cannot be more than one PlayerInput script.  The instances are " + s_Instance.name + " and " + name + ".");
        }
    }

    void Update()
    {
        m_Movement.Set(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        m_Camera.Set(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        var aimTemp = m_Aim;

        m_Aim      = Input.GetButton("Fire2");
        m_Jump     = Input.GetButton("Jump");
        m_Sprint   = Input.GetButton("Shift");
        m_RageMode = Input.GetButton("Skill1");
        m_Push     = Input.GetButton("Skill2");
        m_Might    = Input.GetButton("Skill3");
        m_Pause    = Input.GetButtonDown("Pause");
        m_Skills   = Input.GetButtonDown("Skills");

        if (m_Aim != aimTemp)
        {
            if (m_Aim) OnAimEnable.Invoke();
            else       OnAimDisable.Invoke();
        }

        if (Input.GetButton("Fire1") && m_Aim)
        {
            if (Time.time > nextTimeToFire)
            {
                var atkSpeed = Mathf.Max(1f, PlayerStats.AttackSpeed.Value / 10);
                nextTimeToFire = Time.time + 1f / atkSpeed;

                if (PlayerStats.IsBurstActive)
                    StartCoroutine(FireBurst(3, 300));
                else
                    ToggleShoot(true);
            }
        }
        else if (Input.GetButtonDown("Fire1"))
        {
            if (m_AttackWaitCoroutine != null)
                StopCoroutine(m_AttackWaitCoroutine);

            m_AttackWaitCoroutine = StartCoroutine(AttackWait());
        }

        if (m_Pause)
        {
            if (pml.paused)
            {
                pml.Resume();
            }

            else if(!pml.paused /*&& !spc.isToggled*/)
            {
                pml.Pause();
            }
        }

        /*if (m_Skills)
        {
            if (spc.isToggled)
            {
                spc.Disable();
            }

            else if (!spc.isToggled && !pml.paused)
            {
                spc.Enable();
            }
        }*/

        if (m_RageMode)
        {
            RageMode.Activate();
        }

        if (m_Might)
        {
            ElementalMight.Activate();
        }

        if (m_Push)
        {
            Push.Activate();
        }
    }

    public IEnumerator FireBurst(int burstSize, float rateOfFire)
    {
        float bulletDelay = 60 / rateOfFire;
        // rate of fire in weapons is in rounds per minute (RPM), therefore we should calculate how much time passes before firing a new round in the same burst.
        for (int i = 0; i < burstSize; i++)
        {
            ToggleShoot(true);
            yield return new WaitForSeconds(bulletDelay); // wait till the next round
        }
    }

    IEnumerator AttackWait()
    {
        m_Attack = true;

        yield return m_AttackInputWait;

        m_Attack = false;
    }

    public void ToggleShoot(bool value)
    {
        m_Shoot = value;
    }

    public bool HaveControl()
    {
        return !m_ExternalInputBlocked;
    }

    public void ReleaseControl()
    {
        m_ExternalInputBlocked = true;
    }

    public void GainControl()
    {
        m_ExternalInputBlocked = false;
    }
}
