﻿using UnityEngine;

namespace Gamekit3D
{
    public abstract class Projectile : MonoBehaviour, IPooled<Projectile>
    {
        public int poolID { get; set; }
        public bool shamanFire { get; set; }
        public GameObject collisionSpawn;
        public EnemyStatController enemyStatControllerRef;

        public ObjectPooler<Projectile> pool { get; set; }

        public abstract void Shot(Vector3 target, RangeWeapon shooter);
    }
}