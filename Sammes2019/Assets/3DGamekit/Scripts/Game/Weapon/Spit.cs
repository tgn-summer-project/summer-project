﻿using UnityEngine;

namespace Gamekit3D
{
    public class Spit : GrenadierGrenade
    {
        bool spawned = false;
        protected override void OnCollisionEnter(Collision other)
        {
            base.OnCollisionEnter(other);

            if (shamanFire && gameObject.name == "Shamahalogu(Clone)")
            {
                //The projectile was fired by a Shaman, spawn his minions
                Instantiate(collisionSpawn, transform.position, transform.rotation);
                gameObject.SetActive(false);
            }

            if(gameObject.name == "FrostMissileBlue(Clone)" && other.gameObject.layer == 23)
                gameObject.SetActive(false);

            if (explosionTimer < 0)
                Explosion();
        }
    }
}